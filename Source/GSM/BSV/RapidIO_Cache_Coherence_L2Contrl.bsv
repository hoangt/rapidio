/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIO_Cache_Coherence_L2Contrl;

import RapidIO_Cache_Coherence_Types::*;
import RapidIO_Cache_Coherence_Encode_packet::*;
import DefaultValue::*;

import ConfigReg::*;
import DReg::*;
import RegFile::*;





interface Ifc_L2_CacheContrl;


//method Action _inputs_packet_L2Contrl(Packet pkt);
method Action _inputs_Processor_Agent_Is_Ready(Bool value);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt);
method Action _inputs_packet_To_L2Contrl_From_processor_Agent_Resp(Packet pkt);

method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(Packet pkt);
method Action _load_From_Memory_And_Change_State(Physical_Address_Type physical_Address,State s);

method Packet _outputs_Packet_L2Contrl_To_Processor_Agent_Resp();

//method Action _inputs_next_request();
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt); 

method Packet _get_data_L2Contrl(Packet pkt);
method Packet _outputs_Packet_L2Contrl();
method State  _gets_status_block(Physical_Address_Type physical_Address);
//method Packet _outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // At Remote
method Packet _outputs_data_mkTb(); 
method Bool _outputs_AlterCalled();
//method Packet _outputs_Request_To_Processor_Agent();

 // Local Response

method Action _putRequestFromProcessor(Packet pkt);

method ActionValue#(Packet) _inputs_packet_L2Contrl_From_RapidIO_Req(Packet pkt); // At Remote.
method ActionValue#(Packet) _outputs_Request_To_Processor_Agent();
method Action _inputs_From_Processor_Agent_To_L2contrl_alterState(Packet pkt);


endinterface


`define _max_instructions	26	// Should be 1 less than actual
`define _address_width_rw	64
`define _address_line		[63:0]



(* synthesize *)
(*always_enabled *)
(* always_ready*)
module mkL2CacheContrl#(Bit#(2) l2ID, parameter String dataFileAsCache)(Ifc_L2_CacheContrl);


Wire#(Packet) wr_packetFromL2Cache <- mkDWire(defaultValue);
Wire#(Packet) wr_packetToAlter <- mkDWire(defaultValue);
Wire#(State) wr_state  <- mkDWire(InValid);

// Request and Response Packets to Communicate Processor Agent// 
Wire#(Packet) wr_Request_To_Processor_Agent <- mkDWire(defaultValue);
Wire#(Packet) wr_Response_From_Processor_Agent <- mkDWire(defaultValue);
Wire#(Packet) wr_Response_From_Processor_Agent_Resp <- mkDWire(defaultValue);

Wire#(Packet) wr_AletPacket_From_processor_Agent <- mkDWire(defaultValue);

Wire#(Packet) wr_Response_From_Processor_Agent_Resp_Local <- mkReg(defaultValue);
Wire#(Packet) wr_Response_From_Processor_Agent_Resp_Remote <- mkReg(defaultValue);

Wire#(Packet) wr_Request_From_Processor_Agent_Remote <- mkDWire(defaultValue);// wr_Request_From_Processor_Agent --> Request at remote end; 

Wire#(Packet) wr_Response_To_Processor_Agent <- mkDWire(defaultValue); // wr_Response_To_Processor_Agent --> Response at Remote.

Reg#(Bool)   rg_alterMethodCalled <- mkReg(False);
Wire#(Bool)  wr_Agent_Ready_Put_Request <- mkDWire(False);

ConfigReg#(Physical_Address_Type) reqIndex <- mkConfigReg(0); // reqProcessedNo to keep track of Indexes of regfile




//RegFile#(Physical_Address_Type,Packet)		 requestDataFile <- mkRegFileLoad("packetsRequestData1.txt",'d0 ,`_max_instructions);
RegFile#(Physical_Address_Type,Packet)		 requestDataFile <- mkRegFileLoad(dataFileAsCache,'d0 ,`_max_instructions);




rule rl_put_request_to_processor_agent;
	
		
	wr_Request_To_Processor_Agent <= requestDataFile.sub(reqIndex);
	
	reqIndex <= reqIndex + 1;

endrule

/*
rule disp;
	$display("%x ",wr_Request_To_Processor_Agent);
endrule

*/
/*
rule rl_update_regFile(wr_AletPacket_From_processor_Agent.validPacket == True || wr_Response_From_Processor_Agent_Resp.validPacket == True || wr_Response_From_Processor_Agent_Resp_Local.validPacket == True || wr_Response_From_Processor_Agent_Resp_Remote.validPacket == True || wr_Request_From_Processor_Agent_Remote.validPacket == True );

	

	if(wr_Response_From_Processor_Agent_Resp.validPacket == True)begin
		//requestDataFile.upd(wr_Response_From_Processor_Agent_Resp.physical_Address, wr_Response_From_Processor_Agent_Resp);
	end
	else if(wr_Response_From_Processor_Agent_Resp_Local.validPacket == True)begin
		//requestDataFile.upd(wr_Response_From_Processor_Agent_Resp_Local.physical_Address, wr_Response_From_Processor_Agent_Resp_Local);
		$display("Value received from agent is \n");
		$display("Address= %h,  State = %b ",wr_Response_From_Processor_Agent_Resp_Local.physical_Address,wr_Response_From_Processor_Agent_Resp_Local.state);
	end
	
	else if(wr_AletPacket_From_processor_Agent.validPacket == True)begin
		//requestDataFile.upd(wr_AletPacket_From_processor_Agent.physical_Address, wr_AletPacket_From_processor_Agent);
		$display("Value received from agent is \n");
		$display("Address= %h,  State = %b ",wr_AletPacket_From_processor_Agent.physical_Address,wr_AletPacket_From_processor_Agent.state);
	end
	else if(wr_Response_From_Processor_Agent_Resp_Remote.validPacket == True)begin
		//requestDataFile.upd(wr_Response_From_Processor_Agent_Resp_Remote.physical_Address, wr_Response_From_Processor_Agent_Resp_Remote);
		$display("Value received from agent is \n");
		$display("Address= %h,  State = %b ",wr_Response_From_Processor_Agent_Resp_Remote.physical_Address,wr_Response_From_Processor_Agent_Resp_Remote.state);
		
	end
	else if(wr_Request_From_Processor_Agent_Remote.validPacket == True )begin
		//wr_Response_To_Processor_Agent <= requestDataFile.sub(wr_Request_From_Processor_Agent_Remote.physical_Address);
		
		$display("Value received from agent is \n");
		$display("Address= %h,  State = %b ",wr_Request_From_Processor_Agent_Remote.physical_Address,wr_Request_From_Processor_Agent_Remote.state);
	end
endrule
*/


/*
method Action _inputs_packet_L2Contrl(Packet pkt);

endmethod


method Packet _outputs_Packet_L2Contrl();
    return pkt;
endmethod


*/




method Action _load_From_Memory_And_Change_State(Physical_Address_Type physical_Address,State s);

	


endmethod

//method Action _inputs_packet_L2Contrl(Packet pkt); // Finally this  Packets to Processor Agent.

//	wr_packetFromL2Cache <= pkt;

//endmethod

/*
method Action _putRequestFromProcessor(Packet pkt);
	
	wr_Request_To_Processor_Agent <= pkt;
	
endmethod
*/

method Action _inputs_Processor_Agent_Is_Ready(Bool value);
	wr_Agent_Ready_Put_Request <= value;

endmethod

//method Packet _outputs_Request_To_Processor_Agent();
	
//endmethod

method Action _inputs_packet_To_L2Contrl_From_processor_Agent_Resp(Packet pkt);

	wr_Response_From_Processor_Agent <= pkt;

endmethod

method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
	wr_Response_From_Processor_Agent_Resp_Local <= pkt;
endmethod
method ActionValue#(Packet) _inputs_packet_L2Contrl_From_RapidIO_Req(Packet pkt);
//	wr_Request_From_Processor_Agent_Remote <= pkt;

	return requestDataFile.sub(pkt.physical_Address);
endmethod



method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt); // Modify the L2 Cache.

	wr_Response_From_Processor_Agent_Resp <= pkt;
	
	requestDataFile.upd(pkt.physical_Address, pkt);

endmethod

method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(Packet pkt);
	wr_Response_From_Processor_Agent_Resp_Remote <= pkt;
endmethod

method Action _inputs_From_Processor_Agent_To_L2contrl_alterState(Packet pkt);
//	requestDataFile.upd(pkt.physical_Address,pkt);

	wr_AletPacket_From_processor_Agent <= pkt;

	requestDataFile.upd(pkt.physical_Address, pkt);
	
endmethod

/*
method Packet _updateCache();

	return (wr_packetToAlter.validPacket)? (wr_packetToAlter):(defaultValue);
	
endmethod
*/

/*
method  Packet  _outputs_data_mkTb(); 

	return requestDataFile.sub(physical_Address);

endmethod
*/

method Bool _outputs_AlterCalled();
	return rg_alterMethodCalled;
endmethod

method Packet _outputs_Packet_L2Contrl_To_Processor_Agent_Resp();
		
	return wr_Response_To_Processor_Agent;
endmethod

//method Action _inputs_next_request();
//	reqIndex <= reqIndex + 1;
//endmethod 

method ActionValue#(Packet) _outputs_Request_To_Processor_Agent();
	reqIndex <= reqIndex + 1;
	return wr_Request_To_Processor_Agent;
endmethod





endmodule



endpackage

