/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_Cache_Coherence_mkTopModule;

import ConfigReg::*;
import DReg::*;
import RegFile::*;
import DefaultValue::*;


import RapidIO_Cache_Coherence_Home_Node::*;
import RapidIO_Cache_Coherence_Encode_packet::*;
import RapidIO_Cache_Coherence_Types ::*;
import RapidIO_Cache_Coherence_L2Contrl ::*;
import RapidIO_Cache_Coherence_GSM ::*;
import RapidIO_DTypes::*;
//import RapidIO_Cache_Coherence_L2ControlInterface::*;
import RapidIO_Cache_Coherence_GSM_Interface::*;
import RapidIO_Cache_Coherence_L2Contrl ::*;

//import Ra

`define _max_instructions	13	// Should be 1 less than actual
`define _address_width_rw	64
`define _address_line		[63:0]




/*
interface testBench;

method Action _inputs_testBench_processor1(Packet pkt);
method Action _inputs_testBench_processor2(Packet pkt);
method Action _inputs_testBench_processor3(Packet pkt);
method Action _inputs_testBench_processor4(Packet pkt);
method Packet _output_Packet_Processor_1_To_TestBench_();
method Packet _output_Packet_Processor_2_To_TestBench_();
method Packet _output_Packet_Processor_3_To_TestBench_();
method Packet _output_Packet_Processor_4_To_TestBench_();



endinterface

*/

interface TopModule;

interface Ifc_GSM _ifc_gsm_In_Top1; 
interface Ifc_GSM _ifc_gsm_In_Top2; 
interface Ifc_GSM _ifc_gsm_In_Top3; 
interface Ifc_GSM _ifc_gsm_In_Top4; 
interface Ifc_L2_CacheContrl _ifc_l2; // input from L2 .


endinterface



(* synthesize *)
(* always_enabled *)
(* always_ready*)
module mkCoherenceTopModule(TopModule);

/*
Reg#(Packet)  outputPacketOfProcessor1 <- mkReg(defaultValue);
Reg#(Packet)  outputPacketOfProcessor2 <- mkReg(defaultValue);
Reg#(Packet)  outputPacketOfProcessor3 <- mkReg(defaultValue);
Reg#(Packet)  outputPacketOfProcessor4 <- mkReg(defaultValue);

*/
function Packet func_makePaketValid(Packet pkt, Bool valid );


return Packet{
                pID:pkt.pID,
                physical_Address:pkt.physical_Address,
                isRequest: pkt.isRequest,    // Flag indicate whether it a request or response
                reqType :  pkt.reqType,
                resType :  pkt.resType,
                state   :  pkt.state, 
                validPacket: valid,  // whether data is valid or not
                data: pkt.data


            };

endfunction


Ifc_ProcessorAgent ifc_processorAgent1 <- mkProcessor_Agent(2'd0);
Ifc_ProcessorAgent ifc_processorAgent2 <- mkProcessor_Agent(2'd1);
Ifc_ProcessorAgent ifc_processorAgent3 <- mkProcessor_Agent(2'd2);
Ifc_ProcessorAgent ifc_processorAgent4 <- mkProcessor_Agent(2'd3);

Ifc_L2_CacheContrl ifc_L2Control1 <- mkL2CacheContrl(2'd0,"packetsRequestData1.txt");
Ifc_L2_CacheContrl ifc_L2Control2 <- mkL2CacheContrl(2'd1,"packetsRequestData2.txt");
Ifc_L2_CacheContrl ifc_L2Control3 <- mkL2CacheContrl(2'd2,"packetsRequestData3.txt");
Ifc_L2_CacheContrl ifc_L2Control4 <- mkL2CacheContrl(2'd3,"packetsRequestData4.txt");



/*

Ifc_L2_CacheContrl ifc_L2Control1 <- mkL2CacheContrl(2'd0);
Ifc_L2_CacheContrl ifc_L2Control2 <- mkL2CacheContrl(2'd1);
Ifc_L2_CacheContrl ifc_L2Control3 <- mkL2CacheContrl(2'd2);
Ifc_L2_CacheContrl ifc_L2Control4 <- mkL2CacheContrl(2'd3);
*/

Reg#(Packet ) rg_packetFromAgent <- mkReg(defaultValue);
Reg#(int) count <- mkReg(0);



/*
ConfigReg#(int) count <- mkReg(0);
ConfigReg#(Physical_Address_Type) reqProcessed <- mkConfigReg(0);
ConfigReg#(int) packtSizeCnt1 <- mkConfigReg(0);
ConfigReg#(int) packtSizeCnt2 <- mkConfigReg(0);
ConfigReg#(int) packtSizeCnt3 <- mkConfigReg(0);
ConfigReg#(int) packtSizeCnt4 <- mkConfigReg(0);



// Read data from files.
RegFile#(Physical_Address_Type,Packet)		 dataFileProcessor1 <- mkRegFileLoad("packetsRequestData1.txt",'d0 ,`_max_instructions);
RegFile#(Physical_Address_Type,Packet)		 dataFileProcessor2 <- mkRegFileLoad("packetsRequestData2.txt",'d0 ,`_max_instructions);
RegFile#(Physical_Address_Type,Packet)		 dataFileProcessor3 <- mkRegFileLoad("packetsRequestData3.txt",'d0 ,`_max_instructions);
RegFile#(Physical_Address_Type,Packet)		 dataFileProcessor4 <- mkRegFileLoad("packetsRequestData4.txt",'d0 ,`_max_instructions);
*/
// write result to files

//RegFile#(int,Packet)		 responsePackets <- mkRegFileLoad("dataVirtual.txt",'d0 ,`_max_instructions);




//ifc_processorAgent._outputs_Ready_To_Receive()



ConfigReg#(Packet) dataCounter1 <- mkConfigReg(defaultValue);







rule rl_incrCount;

	count <= count + 1;

endrule


/*
rule rl_putRequestLocal_L2_To_processorAgent;
	
	
endrule:rl_putRequestLocal_L2_To_processorAgent
*/




rule rl_put_PA1_To_L2;
	let pktRespFromPAlocal1 = ifc_processorAgent1._outputs_Resp_PA_TO_L2Contrl_Local();
	let pktRespFromPARemote1 = ifc_processorAgent1._outputs_Resp_PA_TO_L2Contrl_Remote();
	let pktRespFromPAalterState1 = ifc_processorAgent1._outputs_Req_PA_TO_L2_alterState();
	let pktRespFromPALoadAndChange1 = ifc_processorAgent1._outputs_Resp_PA_TO_L2Contrl_LoadAndChange();
	let pktReqFromPAToL2Remote1 = ifc_processorAgent1._outputs_Req_PA_TO_L2_Remote();
	
	if( pktRespFromPAlocal1.validPacket == True)begin
		ifc_L2Control1._inputs_From_Processor_Agent_To_L2contrl_Resp_Local(pktRespFromPAlocal1);
	
	end
	else if(pktRespFromPAalterState1.validPacket == True)begin
		ifc_L2Control1._inputs_From_Processor_Agent_To_L2contrl_alterState(pktRespFromPAalterState1);
	end
	
	else if(pktRespFromPALoadAndChange1.validPacket == True)begin 
		ifc_L2Control1._load_From_Memory_And_Change_State(pktRespFromPALoadAndChange1.physical_Address,pktRespFromPALoadAndChange1.state);
	end
	
	else if(pktReqFromPAToL2Remote1.validPacket == True )begin
		let pkt  <- ifc_L2Control1._inputs_packet_L2Contrl_From_RapidIO_Req(pktReqFromPAToL2Remote1);
		ifc_processorAgent1._inputs_Packet_Remote_Response( func_makePaketValid( pkt,True));
	end
	
	if(pktRespFromPARemote1.validPacket == True)begin
		ifc_L2Control1._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(pktRespFromPARemote1);
	end
	
	
	
	
endrule


rule rl_put_PA2_To_L2; // For Processor Agent 2
	let pktRespFromPAlocal2 = ifc_processorAgent2._outputs_Resp_PA_TO_L2Contrl_Local();
	let pktRespFromPARemote2 = ifc_processorAgent2._outputs_Resp_PA_TO_L2Contrl_Remote();
	let pktRespFromPAalterState2 = ifc_processorAgent2._outputs_Req_PA_TO_L2_alterState();
	let pktRespFromPALoadAndChange2 = ifc_processorAgent2._outputs_Resp_PA_TO_L2Contrl_LoadAndChange();
	let pktReqFromPAToL2Remote2 = ifc_processorAgent2._outputs_Req_PA_TO_L2_Remote();
	
	if( pktRespFromPAlocal2.validPacket == True)begin
		ifc_L2Control2._inputs_From_Processor_Agent_To_L2contrl_Resp_Local(pktRespFromPAlocal2);
	
	end
	else if(pktRespFromPAalterState2.validPacket == True)begin
		ifc_L2Control2._inputs_From_Processor_Agent_To_L2contrl_alterState(pktRespFromPAalterState2);
	end
	
	else if(pktRespFromPALoadAndChange2.validPacket == True)begin 
		ifc_L2Control2._load_From_Memory_And_Change_State(pktRespFromPALoadAndChange2.physical_Address,pktRespFromPALoadAndChange2.state);
	end
	
	else if(pktReqFromPAToL2Remote2.validPacket == True )begin
		let pkt  <- ifc_L2Control2._inputs_packet_L2Contrl_From_RapidIO_Req(pktReqFromPAToL2Remote2);
		ifc_processorAgent2._inputs_Packet_Remote_Response(  func_makePaketValid( pkt,True));
	end
	
	if(pktRespFromPARemote2.validPacket == True)begin
		ifc_L2Control2._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(pktRespFromPARemote2);
	end
	
	
	
	
endrule


rule rl_put_PA3_To_L2; // For Processor Agent 3
	let pktRespFromPAlocal3 = ifc_processorAgent3._outputs_Resp_PA_TO_L2Contrl_Local();
	let pktRespFromPARemote3 = ifc_processorAgent3._outputs_Resp_PA_TO_L2Contrl_Remote();
	let pktRespFromPAalterState3 = ifc_processorAgent3._outputs_Req_PA_TO_L2_alterState();
	let pktRespFromPALoadAndChange3 = ifc_processorAgent3._outputs_Resp_PA_TO_L2Contrl_LoadAndChange();
	let pktReqFromPAToL2Remote3 = ifc_processorAgent3._outputs_Req_PA_TO_L2_Remote();
	
	if( pktRespFromPAlocal3.validPacket == True)begin
		ifc_L2Control3._inputs_From_Processor_Agent_To_L2contrl_Resp_Local(pktRespFromPAlocal3);
	
	end
	else if(pktRespFromPAalterState3.validPacket == True)begin
		ifc_L2Control3._inputs_From_Processor_Agent_To_L2contrl_alterState(pktRespFromPAalterState3);
	end
	
	else if(pktRespFromPALoadAndChange3.validPacket == True)begin 
		ifc_L2Control3._load_From_Memory_And_Change_State(pktRespFromPALoadAndChange3.physical_Address,pktRespFromPALoadAndChange3.state);
	end
	
	else if(pktReqFromPAToL2Remote3.validPacket == True )begin
		let pkt  <- ifc_L2Control3._inputs_packet_L2Contrl_From_RapidIO_Req(pktReqFromPAToL2Remote3);
		ifc_processorAgent3._inputs_Packet_Remote_Response(  func_makePaketValid( pkt,True));
	end
	
	if(pktRespFromPARemote3.validPacket == True)begin
		ifc_L2Control3._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(pktRespFromPARemote3);
	end
	
	
	
	
endrule


rule rl_put_PA4_To_L2; // For Processor Agent 3
	let pktRespFromPAlocal4 = ifc_processorAgent4._outputs_Resp_PA_TO_L2Contrl_Local();
	let pktRespFromPARemote4 = ifc_processorAgent4._outputs_Resp_PA_TO_L2Contrl_Remote();
	let pktRespFromPAalterState4 = ifc_processorAgent4._outputs_Req_PA_TO_L2_alterState();
	let pktRespFromPALoadAndChange4 = ifc_processorAgent4._outputs_Resp_PA_TO_L2Contrl_LoadAndChange();
	let pktReqFromPAToL2Remote4 = ifc_processorAgent4._outputs_Req_PA_TO_L2_Remote();
	
	if( pktRespFromPAlocal4.validPacket == True)begin
		ifc_L2Control4._inputs_From_Processor_Agent_To_L2contrl_Resp_Local(pktRespFromPAlocal4);
	
	end
	else if(pktRespFromPAalterState4.validPacket == True)begin
		ifc_L2Control4._inputs_From_Processor_Agent_To_L2contrl_alterState(pktRespFromPAalterState4);
	end
	
	else if(pktRespFromPALoadAndChange4.validPacket == True)begin 
		ifc_L2Control4._load_From_Memory_And_Change_State(pktRespFromPALoadAndChange4.physical_Address,pktRespFromPALoadAndChange4.state);
	end
	
	else if(pktReqFromPAToL2Remote4.validPacket == True )begin
		let pkt  <- ifc_L2Control4._inputs_packet_L2Contrl_From_RapidIO_Req(pktReqFromPAToL2Remote4);
		ifc_processorAgent4._inputs_Packet_Remote_Response(  func_makePaketValid( pkt,True));
	end
	
	if(pktRespFromPARemote4.validPacket == True)begin
		ifc_L2Control4._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(pktRespFromPARemote4);
	end
	
	
	
	
endrule


rule rl_L2_T0_PA1;// puts req to Processor Agent.
	
	let pktToProcessorAgentLocal1 <- ifc_L2Control1._outputs_Request_To_Processor_Agent();
	let pktToProcessorAgentRemote1 = ifc_L2Control1._outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // From Remote
	if(pktToProcessorAgentLocal1.validPacket == True)begin
		ifc_processorAgent1._inputs_Packet_Local_Processor_Local(pktToProcessorAgentLocal1);
	end
	
	//else if(pktToProcessorAgentRemote.validPacket == True)begin
	//	ifc_processorAgent1._inputs_Packet_Remote_Response(pktToProcessorAgentRemote);
	//end
	
endrule

rule rl_L2_T0_PA2;// puts req to Processor Agent 2.
	
	let pktToProcessorAgentLocal2 <- ifc_L2Control2._outputs_Request_To_Processor_Agent();
	let pktToProcessorAgentRemote2 = ifc_L2Control2._outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // From Remote
	if(pktToProcessorAgentLocal2.validPacket == True)begin
		ifc_processorAgent2._inputs_Packet_Local_Processor_Local(pktToProcessorAgentLocal2);
	end
	
	//else if(pktToProcessorAgentRemote.validPacket == True)begin
	//	ifc_processorAgent1._inputs_Packet_Remote_Response(pktToProcessorAgentRemote);
	//end
	
endrule


rule rl_L2_T0_PA3;// puts req to Processor Agent.
	
	let pktToProcessorAgentLocal3 <- ifc_L2Control3._outputs_Request_To_Processor_Agent();
	let pktToProcessorAgentRemote3 = ifc_L2Control3._outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // From Remote
	if(pktToProcessorAgentLocal3.validPacket == True)begin
		ifc_processorAgent3._inputs_Packet_Local_Processor_Local(pktToProcessorAgentLocal3);
	end
	
	//else if(pktToProcessorAgentRemote.validPacket == True)begin
	//	ifc_processorAgent1._inputs_Packet_Remote_Response(pktToProcessorAgentRemote);
	//end
	
endrule


rule rl_L2_T0_PA4;// puts req to Processor Agent.
	
	let pktToProcessorAgentLocal4 <- ifc_L2Control4._outputs_Request_To_Processor_Agent();
	let pktToProcessorAgentRemote4 = ifc_L2Control4._outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // From Remote
	if(pktToProcessorAgentLocal4.validPacket == True)begin
		ifc_processorAgent4._inputs_Packet_Local_Processor_Local(pktToProcessorAgentLocal4);
	end
	
	//else if(pktToProcessorAgentRemote.validPacket == True)begin
	//	ifc_processorAgent1._inputs_Packet_Remote_Response(pktToProcessorAgentRemote);
	//end
	
endrule






rule rl_stop;

	if(count > 100)begin
	
	$display("All Packets Processed Successfully");
	$finish(0);
	
	end
	
	/*if(count >= 15 && count <= 60)
	$dumpon();
	else
	$dumpoff();*/

endrule

/*
interface Ifc_RapidIO _ifc_RIO;

method Action _inputs_packet_To_L2Contrl_From_processor_Agent_Resp(Packet pkt);
	rg_PacketResponse_From_Processor_Agent <= pkt;
endmethod

method Packet _outputs_Request_To_Processor_Agent();

endmethod

endinterface
*/


interface Ifc_GSM _ifc_gsm_In_Top1;


	
	///   InitReq Signals as Output. Connecting to Wrapper.  ////////////
method Bool _ireq_sof_n();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_sof_n();
endmethod
	
method Bool _ireq_eof_n();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_eof_n();
endmethod
method Bool _ireq_vld_n();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_vld_n();
endmethod
method Bool _ireq_dsc_n ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_dsc_n();
endmethod
method Action ireq_rdy_n_ (Bool value);
	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.ireq_rdy_n_(False);
endmethod
method TT _ireq_tt();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_tt();
endmethod
method Data _ireq_data ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_data();
endmethod
method Bool _ireq_crf ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_crf();
	
endmethod 
method Prio _ireq_prio ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_prio();
endmethod
method Type _ireq_ftype();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_ftype();
endmethod
method DestId _ireq_dest_id ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_dest_id();
endmethod
method Addr _ireq_addr(); 
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_addr();
endmethod
method Bit#(8) _ireq_hopcount ( );
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_hopcount();
endmethod
method TranId _ireq_tid (); 
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_tid();
endmethod
method Type _ireq_ttype ( );
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_ttype();
endmethod
method ByteCount _ireq_byte_count ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_byte_count();
endmethod
method ByteEn _ireq_byte_en_n ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_byte_en_n();
endmethod
method Bool _ireq_local ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_local();
endmethod

method DoorBell  _ireq_db_info ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_db_info();
endmethod
method MsgLen _ireq_msg_len ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_msg_len();
endmethod

method MsgSeg _ireq_msg_seg ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_msg_seg();
endmethod
method Bit#(6) _ireq_mbox ( );
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_mbox();
endmethod

method Bit#(2) _ireq_letter ();
	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._ireq_letter();
endmethod
	


/// Target Response Signals as Output.  Connecting to Wrapper ////////////////////////////////

 method Bool outputs_tresp_sof_n_ ();
 	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_sof_n_();
endmethod
	
	
 method Bool outputs_tresp_eof_n_ (); 
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_eof_n_();
endmethod
	
 method Bool outputs_tresp_vld_n_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_vld_n_();
endmethod
	
 method Bool outputs_tresp_dsc_n_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_dsc_n_();
endmethod
	
 method Bool _inputs_tresp_rdy_n_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._inputs_tresp_rdy_n_();
endmethod

 method TT outputs_tresp_tt_ (); 
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_tt_();
endmethod
	
 method Data outputs_tresp_data_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_data_();
endmethod
	
 method Bool outputs_tresp_crf_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_crf_();
endmethod
	
 method Prio outputs_tresp_prio_ ( );
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_prio_();
endmethod
	
 method Type outputs_tresp_ftype_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_ftype_();
endmethod
	
 method DestId outputs_tresp_dest_id_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_dest_id_();
endmethod
	
 method Status outputs_tresp_status_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_status_();
endmethod
	
 method TranId outputs_tresp_tid_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_tid_();
endmethod
	
 method Type outputs_tresp_ttype_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_ttype_();
endmethod
	
 method Bool outputs_tresp_no_data_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_no_data_();
endmethod

 method MsgSeg outputs_tresp_msg_seg_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_msg_seg_();
endmethod
	
/* method Bit#(2) outputs_tresp_mbox_ ( );
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_;
endmethod
*/	
 method Mletter  outputs_tresp_letter_ ();
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_letter_();
endmethod
 

/////////////////    Treq Methods /////
 
 method Action treq_sof_n_ (Bool value);
 	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_sof_n_(False);
 endmethod
	
 method Action treq_eof_n_ (Bool value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_eof_n_(False);
 endmethod

	
 method Action treq_vld_n_ (Bool value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_vld_n_(False);
 endmethod

	
 method Bool _treq_rdy_n ();
  	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._treq_rdy_n();
 endmethod

	
 method Action treq_tt_ (TT value); 
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_tt_(value);
 endmethod

	 
 method Action treq_data_ (Data value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_data_(value);
 endmethod

	
 method Action treq_crf_ (Bool value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_crf_(value);
 endmethod

	
 method Action treq_prio_ (Prio value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_prio_(value);
 endmethod

	
 method Action treq_ftype_ (Type value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_ftype_(4'b0010);
 endmethod

	
 method Action treq_dest_id_ (DestId value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_dest_id_(value);
 endmethod

	
 method Action treq_source_id_ (SourceId value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_source_id_(value);
 endmethod

	
 method Action treq_tid_ (TranId value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_tid_(value);
 endmethod

	
 method Action treq_ttype_ (Type value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_ttype_(4'b1011);
 endmethod

	
 method Action treq_addr_ (Addr value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_addr_(value);
 endmethod

	
 method Action treq_byte_count_ (ByteCount value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_byte_count_(value);
 endmethod

	
 method Action treq_byte_en_n_ (ByteEn value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.treq_byte_en_n_(value);
 endmethod

	
	
	
//////////////////    Iresp Methods  ///////////////



//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value); 
 	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_sof_n_(value);
 endmethod	
 method Action iresp_eof_n_ (Bool value); 
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_eof_n_(value);
 endmethod	

 method Action iresp_vld_n_ (Bool value);	
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_vld_n_(value);
 endmethod	

 method Bool _iresp_rdy_n ();
  	return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent._iresp_rdy_n();
 endmethod	
 	

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value);  	
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_tt_(value);
 endmethod	

 method Action iresp_data_ (Data value); 	
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_data_(value);
 endmethod	

 method Action iresp_crf_ (Bool value); 
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_crf_(value);
 endmethod	

 method Action iresp_prio_ (Prio value);	
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_prio_(value);
 endmethod	

 method Action iresp_ftype_ (Type value); 	 
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_ftype_(value);
 endmethod	

 method Action iresp_dest_id_ (DestId value); 	
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_dest_id_(value);
 endmethod	

 method Action iresp_source_id_ (SourceId value); 	
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_source_id_(value);
 endmethod	

 method Action iresp_tid_ (TranId value); 
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_tid_(value);
 endmethod	

 method Action iresp_ttype_ (Type value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_ttype_(value);
 endmethod	

 method Action iresp_addr_ (Addr value); 	
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_addr_(value);
 endmethod	

 method Action iresp_byte_count_ (ByteCount value); 
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_byte_count_(value);
 endmethod	

 method Action iresp_byte_en_n_ (ByteEn value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_byte_en_n_(value);
 endmethod	

 method Action iresp_status_(Status value); 
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_status_(value);
 endmethod	

 method Action iresp_local_(Bool value);
  	ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.iresp_local_(value);
 endmethod	

 



endinterface:_ifc_gsm_In_Top1


interface Ifc_GSM _ifc_gsm_In_Top2;


	
	///   InitReq Signals as Output. Connecting to Wrapper.  ////////////
method Bool _ireq_sof_n();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_sof_n();
endmethod
	
method Bool _ireq_eof_n();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_eof_n();
endmethod
method Bool _ireq_vld_n();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_vld_n();
endmethod
method Bool _ireq_dsc_n ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_dsc_n();
endmethod
method Action ireq_rdy_n_ (Bool value);
	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.ireq_rdy_n_(False);
endmethod
method TT _ireq_tt();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_tt();
endmethod
method Data _ireq_data ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_data();
endmethod
method Bool _ireq_crf ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_crf();
	
endmethod 
method Prio _ireq_prio ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_prio();
endmethod
method Type _ireq_ftype();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_ftype();
endmethod
method DestId _ireq_dest_id ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_dest_id();
endmethod
method Addr _ireq_addr(); 
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_addr();
endmethod
method Bit#(8) _ireq_hopcount ( );
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_hopcount();
endmethod
method TranId _ireq_tid (); 
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_tid();
endmethod
method Type _ireq_ttype ( );
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_ttype();
endmethod
method ByteCount _ireq_byte_count ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_byte_count();
endmethod
method ByteEn _ireq_byte_en_n ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_byte_en_n();
endmethod
method Bool _ireq_local ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_local();
endmethod

method DoorBell  _ireq_db_info ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_db_info();
endmethod
method MsgLen _ireq_msg_len ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_msg_len();
endmethod

method MsgSeg _ireq_msg_seg ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_msg_seg();
endmethod
method Bit#(6) _ireq_mbox ( );
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_mbox();
endmethod

method Bit#(2) _ireq_letter ();
	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._ireq_letter();
endmethod
	


/// Target Response Signals as Output.  Connecting to Wrapper ////////////////////////////////

 method Bool outputs_tresp_sof_n_ ();
 	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_sof_n_();
endmethod
	
	
 method Bool outputs_tresp_eof_n_ (); 
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_eof_n_();
endmethod
	
 method Bool outputs_tresp_vld_n_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_vld_n_();
endmethod
	
 method Bool outputs_tresp_dsc_n_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_dsc_n_();
endmethod
	
 method Bool _inputs_tresp_rdy_n_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._inputs_tresp_rdy_n_();
endmethod

 method TT outputs_tresp_tt_ (); 
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_tt_();
endmethod
	
 method Data outputs_tresp_data_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_data_();
endmethod
	
 method Bool outputs_tresp_crf_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_crf_();
endmethod
	
 method Prio outputs_tresp_prio_ ( );
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_prio_();
endmethod
	
 method Type outputs_tresp_ftype_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_ftype_();
endmethod
	
 method DestId outputs_tresp_dest_id_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_dest_id_();
endmethod
	
 method Status outputs_tresp_status_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_status_();
endmethod
	
 method TranId outputs_tresp_tid_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_tid_();
endmethod
	
 method Type outputs_tresp_ttype_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_ttype_();
endmethod
	
 method Bool outputs_tresp_no_data_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_no_data_();
endmethod

 method MsgSeg outputs_tresp_msg_seg_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_msg_seg_();
endmethod
	
/* method Bit#(2) outputs_tresp_mbox_ ( );
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_;
endmethod
*/	
 method Mletter  outputs_tresp_letter_ ();
 return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.outputs_tresp_letter_();
endmethod
 

/////////////////    Treq Methods /////
 
 method Action treq_sof_n_ (Bool value);
 	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_sof_n_(False);
 endmethod
	
 method Action treq_eof_n_ (Bool value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_eof_n_(False);
 endmethod

	
 method Action treq_vld_n_ (Bool value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_vld_n_(False);
 endmethod

	
 method Bool _treq_rdy_n ();
  	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._treq_rdy_n();
 endmethod

	
 method Action treq_tt_ (TT value); 
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_tt_(value);
 endmethod

	 
 method Action treq_data_ (Data value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_data_(value);
 endmethod

	
 method Action treq_crf_ (Bool value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_crf_(value);
 endmethod

	
 method Action treq_prio_ (Prio value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_prio_(value);
 endmethod

	
 method Action treq_ftype_ (Type value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_ftype_(4'b0010);
 endmethod

	
 method Action treq_dest_id_ (DestId value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_dest_id_(value);
 endmethod

	
 method Action treq_source_id_ (SourceId value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_source_id_(value);
 endmethod

	
 method Action treq_tid_ (TranId value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_tid_(value);
 endmethod

	
 method Action treq_ttype_ (Type value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_ttype_(4'b0100);
 endmethod

	
 method Action treq_addr_ (Addr value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_addr_(value);
 endmethod

	
 method Action treq_byte_count_ (ByteCount value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_byte_count_(value);
 endmethod

	
 method Action treq_byte_en_n_ (ByteEn value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.treq_byte_en_n_(value);
 endmethod

	
	
	
//////////////////    Iresp Methods  ///////////////



//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value); 
 	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_sof_n_(value);
 endmethod	
 method Action iresp_eof_n_ (Bool value); 
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_eof_n_(value);
 endmethod	

 method Action iresp_vld_n_ (Bool value);	
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_vld_n_(value);
 endmethod	

 method Bool _iresp_rdy_n ();
  	return ifc_processorAgent2._ifc_gsm_In_ProcessorAgent._iresp_rdy_n();
 endmethod	
 	

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value);  	
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_tt_(value);
 endmethod	

 method Action iresp_data_ (Data value); 	
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_data_(value);
 endmethod	

 method Action iresp_crf_ (Bool value); 
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_crf_(value);
 endmethod	

 method Action iresp_prio_ (Prio value);	
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_prio_(value);
 endmethod	

 method Action iresp_ftype_ (Type value); 	 
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_ftype_(value);
 endmethod	

 method Action iresp_dest_id_ (DestId value); 	
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_dest_id_(value);
 endmethod	

 method Action iresp_source_id_ (SourceId value); 	
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_source_id_(value);
 endmethod	

 method Action iresp_tid_ (TranId value); 
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_tid_(value);
 endmethod	

 method Action iresp_ttype_ (Type value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_ttype_(value);
 endmethod	

 method Action iresp_addr_ (Addr value); 	
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_addr_(value);
 endmethod	

 method Action iresp_byte_count_ (ByteCount value); 
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_byte_count_(value);
 endmethod	

 method Action iresp_byte_en_n_ (ByteEn value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_byte_en_n_(value);
 endmethod	

 method Action iresp_status_(Status value); 
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_status_(value);
 endmethod	

 method Action iresp_local_(Bool value);
  	ifc_processorAgent2._ifc_gsm_In_ProcessorAgent.iresp_local_(value);
 endmethod	

 



endinterface:_ifc_gsm_In_Top2






interface Ifc_GSM _ifc_gsm_In_Top3;


	
	///   InitReq Signals as Output. Connecting to Wrapper.  ////////////
method Bool _ireq_sof_n();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_sof_n();
endmethod
	
method Bool _ireq_eof_n();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_eof_n();
endmethod
method Bool _ireq_vld_n();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_vld_n();
endmethod
method Bool _ireq_dsc_n ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_dsc_n();
endmethod
method Action ireq_rdy_n_ (Bool value);
	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.ireq_rdy_n_(value);
endmethod
method TT _ireq_tt();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_tt();
endmethod
method Data _ireq_data ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_data();
endmethod
method Bool _ireq_crf ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_crf();
	
endmethod 
method Prio _ireq_prio ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_prio();
endmethod
method Type _ireq_ftype();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_ftype();
endmethod
method DestId _ireq_dest_id ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_dest_id();
endmethod
method Addr _ireq_addr(); 
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_addr();
endmethod
method Bit#(8) _ireq_hopcount ( );
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_hopcount();
endmethod
method TranId _ireq_tid (); 
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_tid();
endmethod
method Type _ireq_ttype ( );
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_ttype();
endmethod
method ByteCount _ireq_byte_count ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_byte_count();
endmethod
method ByteEn _ireq_byte_en_n ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_byte_en_n();
endmethod
method Bool _ireq_local ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_local();
endmethod

method DoorBell  _ireq_db_info ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_db_info();
endmethod
method MsgLen _ireq_msg_len ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_msg_len();
endmethod

method MsgSeg _ireq_msg_seg ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_msg_seg();
endmethod
method Bit#(6) _ireq_mbox ( );
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_mbox();
endmethod

method Bit#(2) _ireq_letter ();
	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._ireq_letter();
endmethod
	


/// Target Response Signals as Output.  Connecting to Wrapper ////////////////////////////////

 method Bool outputs_tresp_sof_n_ ();
 	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_sof_n_();
endmethod
	
	
 method Bool outputs_tresp_eof_n_ (); 
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_eof_n_();
endmethod
	
 method Bool outputs_tresp_vld_n_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_vld_n_();
endmethod
	
 method Bool outputs_tresp_dsc_n_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_dsc_n_();
endmethod
	
 method Bool _inputs_tresp_rdy_n_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._inputs_tresp_rdy_n_();
endmethod

 method TT outputs_tresp_tt_ (); 
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_tt_();
endmethod
	
 method Data outputs_tresp_data_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_data_();
endmethod
	
 method Bool outputs_tresp_crf_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_crf_();
endmethod
	
 method Prio outputs_tresp_prio_ ( );
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_prio_();
endmethod
	
 method Type outputs_tresp_ftype_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_ftype_();
endmethod
	
 method DestId outputs_tresp_dest_id_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_dest_id_();
endmethod
	
 method Status outputs_tresp_status_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_status_();
endmethod
	
 method TranId outputs_tresp_tid_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_tid_();
endmethod
	
 method Type outputs_tresp_ttype_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_ttype_();
endmethod
	
 method Bool outputs_tresp_no_data_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_no_data_();
endmethod

 method MsgSeg outputs_tresp_msg_seg_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_msg_seg_();
endmethod
	
/* method Bit#(2) outputs_tresp_mbox_ ( );
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_;
endmethod
*/	
 method Mletter  outputs_tresp_letter_ ();
 return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.outputs_tresp_letter_();
endmethod
 

/////////////////    Treq Methods /////
 
 method Action treq_sof_n_ (Bool value);
 	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_sof_n_(value);
 endmethod
	
 method Action treq_eof_n_ (Bool value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_eof_n_(value);
 endmethod

	
 method Action treq_vld_n_ (Bool value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_vld_n_(value);
 endmethod

	
 method Bool _treq_rdy_n ();
  	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._treq_rdy_n();
 endmethod

	
 method Action treq_tt_ (TT value); 
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_tt_(value);
 endmethod

	 
 method Action treq_data_ (Data value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_data_(value);
 endmethod

	
 method Action treq_crf_ (Bool value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_crf_(value);
 endmethod

	
 method Action treq_prio_ (Prio value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_prio_(value);
 endmethod

	
 method Action treq_ftype_ (Type value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_ftype_(value);
 endmethod

	
 method Action treq_dest_id_ (DestId value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_dest_id_(value);
 endmethod

	
 method Action treq_source_id_ (SourceId value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_source_id_(value);
 endmethod

	
 method Action treq_tid_ (TranId value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_tid_(value);
 endmethod

	
 method Action treq_ttype_ (Type value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_ttype_(value);
 endmethod

	
 method Action treq_addr_ (Addr value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_addr_(value);
 endmethod

	
 method Action treq_byte_count_ (ByteCount value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_byte_count_(value);
 endmethod

	
 method Action treq_byte_en_n_ (ByteEn value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.treq_byte_en_n_(value);
 endmethod

	
	
	
//////////////////    Iresp Methods  ///////////////



//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value); 
 	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_sof_n_(value);
 endmethod	
 method Action iresp_eof_n_ (Bool value); 
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_eof_n_(value);
 endmethod	

 method Action iresp_vld_n_ (Bool value);	
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_vld_n_(value);
 endmethod	

 method Bool _iresp_rdy_n ();
  	return ifc_processorAgent3._ifc_gsm_In_ProcessorAgent._iresp_rdy_n();
 endmethod	
 	

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value);  	
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_tt_(value);
 endmethod	

 method Action iresp_data_ (Data value); 	
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_data_(value);
 endmethod	

 method Action iresp_crf_ (Bool value); 
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_crf_(value);
 endmethod	

 method Action iresp_prio_ (Prio value);	
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_prio_(value);
 endmethod	

 method Action iresp_ftype_ (Type value); 	 
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_ftype_(value);
 endmethod	

 method Action iresp_dest_id_ (DestId value); 	
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_dest_id_(value);
 endmethod	

 method Action iresp_source_id_ (SourceId value); 	
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_source_id_(value);
 endmethod	

 method Action iresp_tid_ (TranId value); 
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_tid_(value);
 endmethod	

 method Action iresp_ttype_ (Type value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_ttype_(value);
 endmethod	

 method Action iresp_addr_ (Addr value); 	
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_addr_(value);
 endmethod	

 method Action iresp_byte_count_ (ByteCount value); 
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_byte_count_(value);
 endmethod	

 method Action iresp_byte_en_n_ (ByteEn value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_byte_en_n_(value);
 endmethod	

 method Action iresp_status_(Status value); 
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_status_(value);
 endmethod	

 method Action iresp_local_(Bool value);
  	ifc_processorAgent3._ifc_gsm_In_ProcessorAgent.iresp_local_(value);
 endmethod	

 



endinterface:_ifc_gsm_In_Top3







interface Ifc_GSM _ifc_gsm_In_Top4;


	
	///   InitReq Signals as Output. Connecting to Wrapper.  ////////////
method Bool _ireq_sof_n();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_sof_n();
endmethod
	
method Bool _ireq_eof_n();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_eof_n();
endmethod
method Bool _ireq_vld_n();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_vld_n();
endmethod
method Bool _ireq_dsc_n ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_dsc_n();
endmethod
method Action ireq_rdy_n_ (Bool value);
	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.ireq_rdy_n_(False);
endmethod
method TT _ireq_tt();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_tt();
endmethod
method Data _ireq_data ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_data();
endmethod
method Bool _ireq_crf ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_crf();
	
endmethod 
method Prio _ireq_prio ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_prio();
endmethod
method Type _ireq_ftype();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_ftype();
endmethod
method DestId _ireq_dest_id ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_dest_id();
endmethod
method Addr _ireq_addr(); 
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_addr();
endmethod
method Bit#(8) _ireq_hopcount ( );
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_hopcount();
endmethod
method TranId _ireq_tid (); 
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_tid();
endmethod
method Type _ireq_ttype ( );
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_ttype();
endmethod
method ByteCount _ireq_byte_count ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_byte_count();
endmethod
method ByteEn _ireq_byte_en_n ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_byte_en_n();
endmethod
method Bool _ireq_local ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_local();
endmethod

method DoorBell  _ireq_db_info ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_db_info();
endmethod
method MsgLen _ireq_msg_len ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_msg_len();
endmethod

method MsgSeg _ireq_msg_seg ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_msg_seg();
endmethod
method Bit#(6) _ireq_mbox ( );
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_mbox();
endmethod

method Bit#(2) _ireq_letter ();
	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._ireq_letter();
endmethod
	


/// Target Response Signals as Output.  Connecting to Wrapper ////////////////////////////////

 method Bool outputs_tresp_sof_n_ ();
 	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_sof_n_();
endmethod
	
	
 method Bool outputs_tresp_eof_n_ (); 
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_eof_n_();
endmethod
	
 method Bool outputs_tresp_vld_n_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_vld_n_();
endmethod
	
 method Bool outputs_tresp_dsc_n_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_dsc_n_();
endmethod
	
 method Bool _inputs_tresp_rdy_n_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._inputs_tresp_rdy_n_();
endmethod

 method TT outputs_tresp_tt_ (); 
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_tt_();
endmethod
	
 method Data outputs_tresp_data_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_data_();
endmethod
	
 method Bool outputs_tresp_crf_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_crf_();
endmethod
	
 method Prio outputs_tresp_prio_ ( );
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_prio_();
endmethod
	
 method Type outputs_tresp_ftype_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_ftype_();
endmethod
	
 method DestId outputs_tresp_dest_id_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_dest_id_();
endmethod
	
 method Status outputs_tresp_status_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_status_();
endmethod
	
 method TranId outputs_tresp_tid_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_tid_();
endmethod
	
 method Type outputs_tresp_ttype_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_ttype_();
endmethod
	
 method Bool outputs_tresp_no_data_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_no_data_();
endmethod

 method MsgSeg outputs_tresp_msg_seg_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_msg_seg_();
endmethod
	
/* method Bit#(2) outputs_tresp_mbox_ ( );
 return ifc_processorAgent1._ifc_gsm_In_ProcessorAgent.outputs_tresp_;
endmethod
*/	
 method Mletter  outputs_tresp_letter_ ();
 return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.outputs_tresp_letter_();
endmethod
 

/////////////////    Treq Methods /////
 
 method Action treq_sof_n_ (Bool value);
 	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_sof_n_(value);
 endmethod
	
 method Action treq_eof_n_ (Bool value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_eof_n_(value);
 endmethod

	
 method Action treq_vld_n_ (Bool value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_vld_n_(value);
 endmethod

	
 method Bool _treq_rdy_n ();
  	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._treq_rdy_n();
 endmethod

	
 method Action treq_tt_ (TT value); 
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_tt_(value);
 endmethod

	 
 method Action treq_data_ (Data value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_data_(value);
 endmethod

	
 method Action treq_crf_ (Bool value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_crf_(value);
 endmethod

	
 method Action treq_prio_ (Prio value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_prio_(value);
 endmethod

	
 method Action treq_ftype_ (Type value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_ftype_(value);
 endmethod

	
 method Action treq_dest_id_ (DestId value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_dest_id_(value);
 endmethod

	
 method Action treq_source_id_ (SourceId value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_source_id_(value);
 endmethod

	
 method Action treq_tid_ (TranId value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_tid_(value);
 endmethod

	
 method Action treq_ttype_ (Type value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_ttype_(value);
 endmethod

	
 method Action treq_addr_ (Addr value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_addr_(value);
 endmethod

	
 method Action treq_byte_count_ (ByteCount value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_byte_count_(value);
 endmethod

	
 method Action treq_byte_en_n_ (ByteEn value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.treq_byte_en_n_(value);
 endmethod

	
	
	
//////////////////    Iresp Methods  ///////////////



//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value); 
 	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_sof_n_(value);
 endmethod	
 method Action iresp_eof_n_ (Bool value); 
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_eof_n_(value);
 endmethod	

 method Action iresp_vld_n_ (Bool value);	
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_vld_n_(value);
 endmethod	

 method Bool _iresp_rdy_n ();
  	return ifc_processorAgent4._ifc_gsm_In_ProcessorAgent._iresp_rdy_n();
 endmethod	
 	

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value);  	
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_tt_(value);
 endmethod	

 method Action iresp_data_ (Data value); 	
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_data_(value);
 endmethod	

 method Action iresp_crf_ (Bool value); 
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_crf_(value);
 endmethod	

 method Action iresp_prio_ (Prio value);	
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_prio_(value);
 endmethod	

 method Action iresp_ftype_ (Type value); 	 
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_ftype_(value);
 endmethod	

 method Action iresp_dest_id_ (DestId value); 	
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_dest_id_(value);
 endmethod	

 method Action iresp_source_id_ (SourceId value); 	
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_source_id_(value);
 endmethod	

 method Action iresp_tid_ (TranId value); 
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_tid_(value);
 endmethod	

 method Action iresp_ttype_ (Type value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_ttype_(value);
 endmethod	

 method Action iresp_addr_ (Addr value); 	
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_addr_(value);
 endmethod	

 method Action iresp_byte_count_ (ByteCount value); 
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_byte_count_(value);
 endmethod	

 method Action iresp_byte_en_n_ (ByteEn value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_byte_en_n_(value);
 endmethod	

 method Action iresp_status_(Status value); 
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_status_(value);
 endmethod	

 method Action iresp_local_(Bool value);
  	ifc_processorAgent4._ifc_gsm_In_ProcessorAgent.iresp_local_(value);
 endmethod	

 



endinterface:_ifc_gsm_In_Top4




/*
interface Ifc_L2_CacheContrl _ifc_l2;
	
method Action _inputs_packet_L2Contrl(Packet pkt);
	
	ifc_L2Control1._inputs_packet_L2Contrl(pkt);

endmethod
//method Action _inputs_Processor_Agent_Is_Ready(Bool value);

//endmethod
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
	ifc_L2Control1._inputs_From_Processor_Agent_To_L2contrl_Resp_Local(ifc_processorAgent1._)
endmethod
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt);
method Action _inputs_next_request();
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt); 
method Action _load_From_Memory_And_Change_State(Physical_Address_Type physical_Address,State s);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(Packet pkt);
method Action _inputs_From_Processor_Agent_To_L2contrl_alterState(Packet pkt);
method Packet _get_data_L2Contrl(Packet pkt);
method Packet _outputs_Packet_L2Contrl();
method State  _gets_status_block(Physical_Address_Type physical_Address);


method Packet _outputs_data_mkTb(); 
method Bool _outputs_AlterCalled();
//method Packet _outputs_Request_To_Processor_Agent();
method Action _inputs_packet_To_L2Contrl_From_processor_Agent_Resp(Packet pkt);
 // Local Response

method Action _inputs_packet_L2Contrl_From_RapidIO_Req(Packet pkt); // At Remote.
method Packet _outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // At Remote
method ActionValue#(Packet) _outputs_Request_To_Processor_Agent();

endmethod

method Action _putRequest_To_PA(Packet value);
	ifc_processorAgent1._inputs_Packet_Local_Processor_Local(ifc_L2Control1._outputs_Request_To_Processor_Agent());
endmethod

endinterface
*/



endmodule




endpackage


// This rule will send request to Processor Agent by Its own, Mimic L2 cintroller request.
/*
rule rl_putRequestProcessorAgent1;

	ifc_processorAgent1._inputs_Packet_Local_Processor( ifc_L2Control1._outputs_data_mkTb());

endrule
*/




/*
/////////////////////////////////////  Rules to get Data from L2 Cache Controller ////////////////////
(* mutually_exclusive = "rl_getDataFromL2Cache1, rl_getReqFromProcessorAgent1" *)
(* mutually_exclusive = "rl_getDataFromL2Cache2, rl_getReqFromProcessorAgent2" *)
(* mutually_exclusive = "rl_getDataFromL2Cache3, rl_getReqFromProcessorAgent3" *)
(* mutually_exclusive = "rl_getDataFromL2Cache4, rl_getReqFromProcessorAgent4" *)
rule rl_getDataFromL2Cache1;
	
	let pkt = ifc_L2Control1._outputs_data_mkTb();
	
	if(pkt.validPacket == True)begin
	
	ifc_processorAgent1._inputs_Packet_Local_Processor((ifc_L2Control1._outputs_data_mkTb());

	end

endrule : rl_getDataFromL2Cache1


rule rl_getDataFromL2Cache2;
	
	let pkt = ifc_L2CacheContrl2._outputs_data_mkTb();
	
	if(pkt.validPacket == True)begin
	
	ifc_processorAgent2._inputs_Packet_Local_Processor((ifc_L2CacheContrl2._outputs_data_mkTb());

	end

endrule : rl_getDataFromL2Cache2




rule rl_getDataFromL2Cache3;

	
	let pkt = ifc_L2CacheContrl3._outputs_data_mkTb();
	
	if(pkt.validPacket == True)begin
	ifc_processorAgent3._inputs_Packet_Local_Processor((ifc_L2CacheContrl3._outputs_data_mkTb());
	end


endrule : rl_getDataFromL2Cache3



rule rl_getDataFromL2Cache4;
	
	
	let pkt = ifc_L2CacheContrl4._outputs_data_mkTb();
	
	if(pkt.validPacket == True)begin
	
		ifc_processorAgent4._inputs_Packet_Local_Processor();
	
	end



endrule : rl_getDataFromL2Cache4
/////////////////////////////////////////////////////////////////////////////////////////////////////////






//This rule will take request from Processor Agent. 
rule rl_getReqFromProcessorAgent1;


	let rg_packetFromAgent1  <= ifc_processorAgent1._outputs_Packet_Req_Local_processor();
	
	if(rg_packetFromAgent1.validPacket == True)begin
		ifc_L2Control1._inputs_Packet_Local_Processor(rg_packetFromAgent1);
	end


endrule


rule rl_getReqFromProcessorAgent2;


	let rg_packetFromAgent2  <= ifc_processorAgent2._outputs_Packet_Req_Local_processor();
	
	if(rg_packetFromAgent2.validPacket == True)begin
		ifc_L2CacheContrl2._inputs_Packet_Local_Processor(rg_packetFromAgent2);
	end	


endrule


rule rl_getReqFromProcessorAgent3;


	let rg_packetFromAgent3  <= ifc_processorAgent3._outputs_Packet_Req_Local_processor();
	
	if(rg_packetFromAgent3.validPacket == True)begin
		ifc_L2CacheContrl3._inputs_Packet_Local_Processor(rg_packetFromAgent3);
	end	


endrule


rule rl_getReqFromProcessorAgent4;


	let rg_packetFromAgent4  <= ifc_processorAgent4._outputs_Packet_Req_Local_processor();
	
	if(rg_packetFromAgent4.validPacket == True)begin
		ifc_L2CacheContrl4._inputs_Packet_Local_Processor(rg_packetFromAgent4);
	end	


endrule


*/



/*

rule rl_getDataForPhysical;

	//Packet pkt;
	$display("Processor 1 ");
	let pkt = dataFileProcessor1.sub(packtSizeCnt1);
	
	let pkt = ifc_L2Control1._outputs_data_mkTb();
	
	$display("Pid = %b,  Physical Address = %b, reqType = %b,  isRequest = %b,  validPacket  = %b \n",pkt.pID,pkt.physical_Address,pkt.reqType,pkt.isRequest,pkt.validPacket);
	
	
	//ifc_processorAgent1._inputs_Packet_Local_Processor(pkt);
	packtSizeCnt1 <= packtSizeCnt1 + 1;

	

endrule

rule rl_putDataProcessorAgent2;

	//Packet pkt;
	let pkt = dataFileProcessor2.sub(packtSizeCnt2);
	
	$display("Processor 2 ");
	$display("Pid = %b,  Physical Address = %b, reqType = %b,  isRequest = %b,  validPacket  = %b \n",pkt.pID,pkt.physical_Address,pkt.reqType,pkt.isRequest,pkt.validPacket);
	
	
	ifc_processorAgent2._inputs_Packet_Local_Processor(pkt);
	packtSizeCnt2 <= packtSizeCnt2 + 1;

	

endrule

rule rl_putDataProcessorAgent3;

	//Packet pkt;
	$display("Processor 3 ");
	let pkt = dataFileProcessor3.sub(packtSizeCnt3);
	
	$display("Pid = %b,  Physical Address = %b, reqType = %b,  isRequest = %b,  validPacket  = %b \n",pkt.pID,pkt.physical_Address,pkt.reqType,pkt.isRequest,pkt.validPacket);
	
	
	ifc_processorAgent3._inputs_Packet_Local_Processor(pkt);
	packtSizeCnt3 <= packtSizeCnt3 + 1;

	

endrule

rule rl_putDataProcessorAgent4;

	//Packet pkt;
	$display("Processor 4 ");
	let pkt = dataFileProcessor4.sub(packtSizeCnt4);
	
	$display("Pid = %b,  Physical Address = %b, reqType = %b,  isRequest = %b,  validPacket  = %b \n",pkt.pID,pkt.physical_Address,pkt.reqType,pkt.isRequest,pkt.validPacket);
	
	
	ifc_processorAgent4._inputs_Packet_Local_Processor(pkt);
	packtSizeCnt4 <= packtSizeCnt4 + 1;
	
	

	

endrule



rule rl_getDataProcessorAgent1;

	
	let pkt = ifc_processorAgent1._outputs_Packet_Local_processor();
	
	if(pkt.validPacket == True)begin
		
			dataFileProcessor1.upd(pkt.physical_Address,pkt.data);
	
		
	
	end




endrule

rule rl_getDataProcessorAgent2;

	
	let pkt = ifc_processorAgent2._outputs_Packet_Local_processor();
	
	if(pkt.validPacket == True)begin
		
			
		dataFileProcessor2.upd(pkt.physical_Address,pkt.data);
		
	
	end




endrule
rule rl_getDataProcessorAgent3;

	
	let pkt = ifc_processorAgent3._outputs_Packet_Local_processor();
	
	if(pkt.validPacket == True)begin
		
			
	dataFileProcessor3.upd(pkt.physical_Address,pkt.data);
		
	
	end




endrule

rule rl_getDataProcessorAgent4;

	
	let pkt = ifc_processorAgent4._outputs_Packet_Local_processor();
	
	if(pkt.validPacket == True)begin
		
			
	
 	  dataFileProcessor4.upd(pkt.physical_Address,pkt.data);
	
	end




endrule




*/


