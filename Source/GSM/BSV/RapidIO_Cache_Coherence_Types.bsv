/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
package RapidIO_Cache_Coherence_Types;


import Vector::*;
import DefaultValue::*;
`include "RapidIO_Cache_Coherence.define"




typedef Bit#(`_physical_address_width) 	Physical_Address_Type;
typedef Bit#(`_tag_width)				Tag_Type;
typedef Bit#(`_block_size)				Data;	
typedef Bit#(4)         Processor_ID;


typedef struct{
Physical_Address_Type physical_Address;
Processor_ID pID;

}CoherenceTID deriving (Bits,Eq);

instance DefaultValue#(CoherenceTID);
    defaultValue = CoherenceTID {physical_Address:50'b0,pID:4'b0};
    
endinstance

//states of MESIF
typedef enum {
InValid,
Shared,
Modified,
Exclusive,
Forwarding,
Owned,
ERROR

}State deriving (Bits,Eq);


    

/*
typedef struct {
State s;
Data  d;	


}Data_From_Home_Node deriving (Bits, Eq);

*/


typedef enum {

Read_Miss,
Read_Hit,
Write_Hit,
Write_Miss,
Invalidate,
InValid


}Request_Type deriving (Bits , Eq);




typedef enum{

DONE,
DataValueReply,
Data_write_back,
//Conflict,
Retry,
InValid




}Response_Type deriving (Bits , Eq);



typedef enum
{
PRL, //Port_Read_Line;
PRIL,//Port_Read_Invalidate_Line its a  kind of request for ownership
PWL//Port write Line

}Signal_From_Requesting_node deriving (Bits, Eq); // Broadcast Signals



typedef enum{

IACK,//Invalid State AcknoWledgement 
SACK,//Shared State AcknoWledgement
DACK,//AcknoWledgement of Data Received 
Conflict

}Response_From_PeerNode_To_Requesting_Node deriving(Bits, Eq);



typedef enum{

Read_Conflict,
CNCL//This message is sent to the Home node in response to a hit in a Peer node and lists all con?icts.


}Message_TO_Home_Node deriving (Bits, Eq);//These messages are transmitted to  the Home node by a Peer node.


typedef enum{

// Data is left;
ACK,
WAIT,
XFR// Transfer


}Message_From_Home_Node deriving (Bits, Eq);//These messages are sent from the Home node to the Peer and/or Requesting nodes.


endpackage
/*Defining the directory entry. Directory info. will be associacted with each block;
typedef struct {
Vector#(_number_of_processor,Bit#(1)) shares;
State s;

}Directory_Entry deriving (Bits, Eq); 

*/

