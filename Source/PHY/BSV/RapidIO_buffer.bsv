/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO buffer Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- A buufer to store and forwrd the packets
--
--To do's
-- Yet to complete
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
package RapidIO_buffer;

import BRAM::*


interface Ifc_RapidIO_buffer_Tx;

//input methods

method Action _tx_sof_n (Bool value);
method Action _tx_eof_n (Bool value);
method Action _tx_vld_n (Bool value);
method Action _tx_dsc_n (Bool value);
method Action _tx_rdy_n (Bool value);

method Action _tx_data_n (Datapkt#(128) value);
method Action _tx_rem_n (Rem#(2) value);
method Action _tx_crf_n (Bool value);
method Action _tx_response_n (Bool value);
method Action _tx_resp_only_n (Bool value);

endinterface : Ifc_RapidIO_buffer_Tx

/*
interface Ifc_RapidIO_buffer_Rx;

//input methods

method Action _rx_sof_n (Bool value);
method Action _rx_eof_n (Bool value);
method Action _rx_vld_n (Bool value);
method Action _tx_dsc_n (Bool value);
method Action _rx_rdy_n (Bool value);

method Action _rx_data_n (Datapkt#(128) value);
method Action _rx_rem_n (Rem#(2) value);
method Action _Rx_crf_n (Bool value);

endinterface : Ifc_RapidIO_buffer_Tx

Wire#(Bool)) wr_rx_sof_n <- mkDWire (False);
Wire#(Bool)) wr_rx_eof_n <- mkDWire (False);
Wire#(Bool)) wr_rx_vld_n <- mkDWire (False);
Wire#(Bool)) wr_rx_dsc_n <- mkDWire (False);
Wire#(Bool)) wr_rx_rdy_n <- mkDWire (False);

Wire#(Bit#(128)) wr_rx_data_n <- mkDWire (0);
Wire#(Bit#(2)) wr_rx_rem_n <- mkDWire (0);
Wire#(Bool)) wr_rx_crf_n <- mkDWire (False);
*/


// output methods
module mkRapidIO_buffer (Ifc_RapidIO_buffer_Tx); // receiver yet to be planned

Wire#(Bool)) wr_tx_sof_n <- mkDWire (False);
Wire#(Bool)) wr_tx_eof_n <- mkDWire (False);
Wire#(Bool)) wr_tx_vld_n <- mkDWire (False);
Wire#(Bool)) wr_tx_dsc_n <- mkDWire (False);
Wire#(Bool)) wr_tx_rdy_n <- mkDWire (False);

Wire#(Bit#(128)) wr_tx_data_n <- mkDWire (0);
Wire#(Bit#(2)) wr_tx_rem_n <- mkDWire (0);
Wire#(Bool)) wr_tx_crf_n <- mkDWire (False);



BRAM_Configure cfg = defaultValue ;
	BRAM2Port#(Bit#(`_sets_of_l2), `_tag_size_plus_1_maybe_type_l2) dcache_tag_bramA <- mkBRAM2Server(cfg);




method Action _tx_sof_n (Bool value);
	wr_tx_sof_n <= value;

method Action _tx_eof_n (Bool value);
	wr_tx_eof_n <= value;

method Action _tx_vld_n (Bool value);
	wr_tx_vld_n <= value;

method Action _tx_dsc_n (Bool value);
	wr_tx_dsc_n <= value;

method Action _tx_rdy_n (Bool value);
	wr_tx_rdy_n <= value;

method Action _tx_data_n (Datapkt#(128) value);
	wr_tx_data_n <= value;

method Action _tx_rem_n (Rem#(2) value);
	wr_tx_rem_n <= value;

method Action _tx_crf_n (Bool value);
	wr_tx_crf_n <= value;


endmodule : mkRapidIO_buffer
endpackage : RapidIO_buffer


