/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Physical Layer CRC-16 Checker Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1. It is used to check the data if its corrupted.
-- 2. Design is implemented as mentioned in the RapidIO specification 3.0. 
-- 
-- To Dos
-- This design will be modified in accordance with the final packet format
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

// To Follow the Endianness, the Bits are modified.
// 1. RapidIO Supports Big Endian and Bluespec Supports Little Endian. 
// So Equation Table is modified to support Little Endian. 

package RapidIO_PhyCRC16Checker;

// Function is used to perform three level operation of CRC-16.
function Bit#(16) fn_CRC16Generation (Bit#(16) old_check_symbol, Bit#(16) data_in);

// 1st Level Operation (XORing Data and Old Symbol)
    Bit#(16) lv_IntermediateSymbol = data_in ^ old_check_symbol; 

// 2nd Level Operation (Equation Network)
    Bit#(1) lv_Check_Symbol_15 = lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[0];
    Bit#(1) lv_Check_Symbol_14 = lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];
    Bit#(1) lv_Check_Symbol_13 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];
    Bit#(1) lv_Check_Symbol_12 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];
    Bit#(1) lv_Check_Symbol_11 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4];
    Bit#(1) lv_Check_Symbol_10 = lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[1];  
    Bit#(1) lv_Check_Symbol_9 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];
    Bit#(1) lv_Check_Symbol_8 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];
    Bit#(1) lv_Check_Symbol_7 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];
    Bit#(1) lv_Check_Symbol_6 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4];
    Bit#(1) lv_Check_Symbol_5 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[5];
    Bit#(1) lv_Check_Symbol_4 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[6];
    Bit#(1) lv_Check_Symbol_3 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[0];
    Bit#(1) lv_Check_Symbol_2 = lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];
    Bit#(1) lv_Check_Symbol_1 = lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];
    Bit#(1) lv_Check_Symbol_0 = lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];

// 3rd Level Operation - Generates Next Check Symbol 
    return {lv_Check_Symbol_0, lv_Check_Symbol_1, lv_Check_Symbol_2, lv_Check_Symbol_3, lv_Check_Symbol_4, lv_Check_Symbol_5, lv_Check_Symbol_6, lv_Check_Symbol_7, lv_Check_Symbol_8, lv_Check_Symbol_9, lv_Check_Symbol_10, lv_Check_Symbol_11, lv_Check_Symbol_12, lv_Check_Symbol_13, lv_Check_Symbol_14, lv_Check_Symbol_15};

endfunction 


interface Ifc_RapidIO_PhyCRC16Checker;
//Input signals
 method Action _link_rx_sof_n (Bool value);
 method Action _link_rx_eof_n (Bool value);
 method Action _link_rx_vld_n (Bool value);
 method Action _link_rx_rem (Bit#(4) value);
 method Action _link_rx_data (Bit#(144) value); //Data[143:16] + Code[15:0]

 method Bit#(16) output_CRC16_check();

endinterface : Ifc_RapidIO_PhyCRC16Checker


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIO_PhyCRC16Checker(Ifc_RapidIO_PhyCRC16Checker);

//Input signals as Wires
Wire#(Bool) wr_rx_sof <- mkDWire (True);
Wire#(Bool) wr_rx_eof <- mkDWire (True);
Wire#(Bool) wr_rx_vld <- mkDWire (True);
Wire#(Bit#(4)) wr_rx_rem <- mkDWire (0);
Wire#(Bit#(144)) wr_rx_data <- mkDWire (0);

Wire#(Bit#(16)) wr_CheckSymbolChk_0 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_1 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_2 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_3 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_4 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_5 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_6 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_7 <- mkDWire (0);

Reg#(Bit#(16)) rg_OldCheckSymbol <- mkReg (0);



rule rl_CRC_16_Checker(wr_rx_vld == False);

Bit#(16) lv_OldCheckSymbol = (wr_rx_sof == False) ? 16'hffff : rg_OldCheckSymbol;
Bit#(16) lv_InitialData = (wr_rx_sof == False) ? {6'h00, wr_rx_data[137:128]} : wr_rx_data[143:128];

Bit#(16) lv_CheckSymbolChk_0 = fn_CRC16Generation (lv_OldCheckSymbol, lv_InitialData);
Bit#(16) lv_CheckSymbolChk_0_1 = fn_CRC16Generation (lv_OldCheckSymbol, wr_rx_data[143:128]);//000 - 143:128
         wr_CheckSymbolChk_0  <= fn_CRC16Generation (lv_CheckSymbolChk_0_1, wr_rx_data[15:0]);//crc code from generation

Bit#(16) lv_CheckSymbolChk_1 = fn_CRC16Generation (lv_CheckSymbolChk_0, wr_rx_data[127:112]);//001 - 143:112
         wr_CheckSymbolChk_1 <= fn_CRC16Generation (lv_CheckSymbolChk_1, wr_rx_data[15:0]);//crc code

Bit#(16) lv_CheckSymbolChk_2 = fn_CRC16Generation (lv_CheckSymbolChk_1, wr_rx_data[111:96]);//010 - 143:96
         wr_CheckSymbolChk_2 <= fn_CRC16Generation (lv_CheckSymbolChk_2, wr_rx_data[15:0]);//crc code

Bit#(16) lv_CheckSymbolChk_3 = fn_CRC16Generation (lv_CheckSymbolChk_2, wr_rx_data[95:80]);//011 - 143:80
         wr_CheckSymbolChk_3 <= fn_CRC16Generation (lv_CheckSymbolChk_3, wr_rx_data[15:0]);//crc code

Bit#(16) lv_CheckSymbolChk_4 = fn_CRC16Generation (lv_CheckSymbolChk_3, wr_rx_data[79:64]);//100 - 143:64
         wr_CheckSymbolChk_4 <= fn_CRC16Generation (lv_CheckSymbolChk_4, wr_rx_data[15:0]);//crc code

Bit#(16) lv_CheckSymbolChk_5 = fn_CRC16Generation (lv_CheckSymbolChk_4, wr_rx_data[63:48]);//101 - 143:48
         wr_CheckSymbolChk_5 <= fn_CRC16Generation (lv_CheckSymbolChk_5, wr_rx_data[15:0]);//crc code

Bit#(16) lv_CheckSymbolChk_6 = fn_CRC16Generation (lv_CheckSymbolChk_5, wr_rx_data[47:32]);//110 - 143:32
         wr_CheckSymbolChk_6 <= fn_CRC16Generation (lv_CheckSymbolChk_6, wr_rx_data[15:0]);//crc code

Bit#(16) lv_CheckSymbolChk_7 = fn_CRC16Generation (lv_CheckSymbolChk_6, wr_rx_data[31:16]);//111 - 143:16
         wr_CheckSymbolChk_7 <= fn_CRC16Generation (lv_CheckSymbolChk_7, wr_rx_data[15:0]);//crc code

rg_OldCheckSymbol <= fn_CRC16Generation (lv_CheckSymbolChk_6, wr_rx_data[31:16]);

endrule 

//Input and Output method definitions
method Action _link_rx_sof_n (Bool value);
    wr_rx_sof <= value;
 endmethod 
 method Action _link_rx_eof_n (Bool value);
    wr_rx_eof <= value;
 endmethod 
 method Action _link_rx_vld_n (Bool value);
    wr_rx_vld <= value; 
 endmethod 
method Action _link_rx_data (Bit#(144) value);
    wr_rx_data <= value;
     endmethod 
method Action _link_rx_rem (Bit#(4) value);
    wr_rx_rem <= value; 
 endmethod 
method Bit#(16) output_CRC16_check();

   if (wr_rx_eof == False) 
   begin
             if (wr_rx_rem[3:1] == 3'b000)
            return wr_CheckSymbolChk_0;
        else if (wr_rx_rem[3:1] == 3'b001)
            return wr_CheckSymbolChk_1;
        else if (wr_rx_rem[3:1] == 3'b010)
            return wr_CheckSymbolChk_2;
        else if (wr_rx_rem[3:1] == 3'b011)
            return wr_CheckSymbolChk_3;
        else if (wr_rx_rem[3:1] == 3'b100)
            return wr_CheckSymbolChk_4;
        else if (wr_rx_rem[3:1] == 3'b101)
            return wr_CheckSymbolChk_5;
        else if (wr_rx_rem[3:1] == 3'b110)
            return wr_CheckSymbolChk_6;
        else 
            return wr_CheckSymbolChk_7;
    end 
    else 
        return 0; 
 endmethod 


endmodule:mkRapidIO_PhyCRC16Checker
endpackage:RapidIO_PhyCRC16Checker
