/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Physical Layer CRC-24 Generation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1. It is used to generate the CRC-24 generation.
-- 2. Design is implemented as mentioned in the RapidIO specification 3.0. 
-- 3. The Control symbol packet format 64 require to generate CRC24.
-- 4. The control symbol packet (38 bits) is used as input data. 
-- 5. Initially CRC-24 check sum value is assigned to all 1's. 
-- 6. Function is developed to perform the CRC-24 generation (as specified in the RapidIO specification).
--
-- Author(s):
-- Gopinathan (gopinathan18@gmail.com)
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIO_PhyCRC24Generation;

// The Endian format is changed from Big Endian to little Endian

function Bit#(24) fn_CRC24Generation (Bit#(38) datain);
    Bit#(1) cksum0 = datain[0] ^ datain[2] ^ datain[5] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[13] ^ datain[14] ^ datain[21] ^ datain[23] ^ datain[24] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[34] ^ datain[36];

    Bit#(1) cksum1 = ~ datain[0] ^ datain[1] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[9] ^ datain[12] ^ datain[13] ^ datain[15] ^ datain[21] ^ datain[22] ^ datain[23] ^ datain[25] ^ datain[26] ^ datain[30] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37];

    Bit#(1) cksum2 = ~ datain[1] ^ datain[2] ^ datain[3] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[10] ^ datain[13] ^ datain[14] ^ datain[16] ^ datain[22] ^ datain[23] ^ datain[24] ^ datain[26] ^ datain[27] ^ datain[31] ^ datain[35] ^ datain[36] ^ datain[37];

    Bit#(1) cksum3 = ~ datain[0] ^ datain[3] ^ datain[4] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[13] ^ datain[15] ^ datain[17] ^ datain[21] ^ datain[25] ^ datain[26] ^ datain[29] ^ datain[32] ^ datain[34] ^ datain[37];

    Bit#(1) cksum4 = ~datain[1] ^ datain[4] ^ datain[5] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[14] ^ datain[16] ^ datain[18] ^ datain[22] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[33] ^ datain[35];

    Bit#(1) cksum5 = ~ datain[2] ^ datain[5] ^ datain[6] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[15] ^ datain[17] ^ datain[19] ^ datain[23] ^ datain[27] ^ datain[28] ^ datain[31] ^ datain[34] ^ datain[36];

    Bit#(1) cksum6 = ~ datain[0] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[9] ^ datain[12] ^ datain[14] ^ datain[16] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[23] ^ datain[26] ^ datain[27] ^ datain[32] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37];

    Bit#(1) cksum7 = datain[0] ^ datain[1] ^ datain[2] ^ datain[3] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[11] ^ datain[14] ^ datain[15] ^ datain[17] ^ datain[19] ^ datain[22] ^ datain[23] ^ datain[26] ^ datain[29] ^ datain[33] ^ datain[34] ^ datain[35] ^ datain[37];

    Bit#(1) cksum8 = ~ datain[0] ^ datain[1] ^ datain[3] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[15] ^ datain[16] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[26] ^ datain[28] ^ datain[29] ^ datain[30] ^ datain[35];

    Bit#(1) cksum9 = datain[1] ^ datain[2] ^ datain[4] ^ datain[5] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[15] ^ datain[16] ^ datain[17] ^ datain[19] ^ datain[21] ^ datain[22] ^ datain[27] ^ datain[29] ^ datain[30] ^ datain[31] ^ datain[36];

    Bit#(1) cksum10 = ~ datain[0] ^ datain[3] ^ datain[6] ^ datain[8] ^ datain[11] ^ datain[15] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[22] ^ datain[24] ^ datain[26] ^ datain[27] ^ datain[29] ^ datain[30] ^ datain[31] ^ datain[32] ^ datain[34] ^ datain[36] ^ datain[37];

    Bit#(1) cksum11 = datain[0] ^ datain[1] ^ datain[2] ^ datain[4] ^ datain[5] ^ datain[7] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[16] ^ datain[17] ^ datain[18] ^ datain[19] ^ datain[22] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[29] ^ datain[30] ^ datain[24] ^ datain[24] ^ datain[33] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37];

    Bit#(1) cksum12 = datain[1] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[8] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[15] ^ datain[17] ^ datain[18] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[31] ^ datain[32] ^ datain[33] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37];

    Bit#(1) cksum13 = datain[0] ^ datain[3] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[15] ^ datain[16] ^ datain[18] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[29] ^ datain[31] ^ datain[32] ^ datain[33] ^ datain[35] ^ datain[37];

    Bit#(1) cksum14 = ~ datain[0] ^ datain[1] ^ datain[2] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[12] ^ datain[14] ^ datain[16] ^ datain[17] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[30] ^ datain[32] ^ datain[33];

    Bit#(1) cksum15 = ~ datain[1] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[13] ^ datain[15] ^ datain[17] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[24] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[30] ^ datain[31] ^ datain[33] ^ datain[34];

    Bit#(1) cksum16 = datain[0] ^ datain[3] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[8] ^ datain[12] ^ datain[13] ^ datain[16] ^ datain[18] ^ datain[19] ^ datain[22] ^ datain[23] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[31] ^ datain[32] ^ datain[35] ^ datain[36];

    Bit#(1) cksum17 = ~ datain[1] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[9] ^ datain[13] ^ datain[14] ^ datain[17] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[31] ^ datain[32] ^ datain[33] ^ datain[36] ^ datain[37];

    Bit#(1) cksum18 = ~ datain[0] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[11] ^ datain[13] ^ datain[15] ^ datain[18] ^ datain[20] ^ datain[23] ^ datain[25] ^ datain[32] ^ datain[33] ^ datain[36] ^ datain[37];

    Bit#(1) cksum19 = ~ datain[0] ^ datain[1] ^ datain[2] ^ datain[5] ^ datain[7] ^ datain[8] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[16] ^ datain[19] ^ datain[23] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[33] ^ datain[36] ^ datain[37];

    Bit#(1) cksum20 = ~ datain[0] ^ datain[1] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[8] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[17] ^ datain[20] ^ datain[21] ^ datain[23] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[36] ^ datain[37];

    Bit#(1) cksum21 = datain[1] ^ datain[2] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[9] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[18] ^ datain[21] ^ datain[22] ^ datain[24] ^ datain[27] ^ datain[28] ^ datain[31] ^ datain[37];

    Bit#(1) cksum22 = datain[0] ^ datain[3] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[11] ^ datain[12] ^ datain[19] ^ datain[21] ^ datain[22] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[32] ^ datain[34] ^ datain[36];

    Bit#(1) cksum23 = datain[1] ^ datain[4] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[12] ^ datain[13] ^ datain[20] ^ datain[22] ^ datain[23] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[33] ^ datain[35] ^ datain[37];

    return {cksum23, cksum22, cksum21, cksum20, cksum19, cksum18, cksum17, cksum16, cksum15, cksum14, cksum13, cksum12, cksum11, cksum10, cksum9, cksum8,cksum7, cksum6, cksum5, cksum4, cksum3, cksum2, cksum1,cksum0};

endfunction


interface Ifc_RapidIO_PhyCRC24Generation;
// -- Input Methods 
method Action _inputs_ControlSymbolData_in (Bit#(38) value);
method Action _inputs_tx_vld (Bool value);

// -- Output Method
method Bit#(24) outputs_CRC24_ ();

endinterface : Ifc_RapidIO_PhyCRC24Generation


(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_PhyCRC24Generation (Ifc_RapidIO_PhyCRC24Generation);
// Input Methods as Wires
Wire#(Bit#(38)) wr_data_in <- mkDWire (0);
Wire#(Bool) wr_tx_vld <- mkDWire (True);
// Output Method as Wires
Wire#(Bit#(24)) wr_check_Sum <- mkDWire (24'hffffff); // By default, as per specification the initial value of CRC is 24'hffffff

/*
rule rl_CRC24;
    if (wr_tx_vld == False) 
        wr_check_Sum <= fn_CRC24Generation (wr_data_in);
    else    
        wr_check_Sum <= 24'hffffff; 
endrule
*/


// Input and Outputs Methods Definitions
method Action _inputs_ControlSymbolData_in (Bit#(38) value);
    wr_data_in <= value;
endmethod

method Action _inputs_tx_vld (Bool value);
    wr_tx_vld <= value;
endmethod

method Bit#(24) outputs_CRC24_ ();
// -- 
    if (wr_tx_vld == False) 
        return fn_CRC24Generation (wr_data_in);
    else    
        return 24'hffffff; 
endmethod


endmodule : mkRapidIO_PhyCRC24Generation
endpackage : RapidIO_PhyCRC24Generation

