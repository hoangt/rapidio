
/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Packet Generation FIFO for Ftype9 Packet Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. In this module, we define the segment generation (Header,data, crc ) and enquing the
-- segments depending upon the piority(class of service) into different queues 
-- and dequeing the segments based on the priority also interleaving the segments.
--
-- To Do's
-- Module Yet Be Verified
--
-- Author(s):
-- S. Yadhava Krishnan M. Guruprasad, R. Ajay Kumar , S. Navin
--
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
/*
Project: RapidIO -> Data Streaming (FIFO Queing based on class of service )
Members: S. Yadhava Krishnan M. Guruprasad, R. Ajay Kumar , S. Navin.
Last Modified: 05/07/2013 10.00 PM

Module Description: In this module we define the segment generation(Header,data, crc ) and enquing the segments depending upon the piority(class of service) into different queues and dequeing the segments based on the priority also interleaving the segments.

*/

package RapidIO_PktGen_FIFO; 		//Package for Data Streaming implementation

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
//import RapidIO_IOPkt_Generation ::*;
import FIFOF ::*;								//Package for implementing queues
import RapidIO_TxFTypeFunctionsDev8 ::*;
`include "RapidIO.defines"

interface Ifc_RapidIO_PktGen_FIFO;
 // Input Ports as Methods
 method Action _inputs_FType9DataStreamingClass(FType9_DataStreamingClass pkt);
 method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt ireqpkt);
// method Action _inputs_ftype9_process(Bool value);
 // Output Ports as Methods
 method DataPkt outputs_Data_toTop_();
 method Bool output_queue_status_();

 //method InitReqIfcCntrl pkgen_ctrl_n_ ();
method InitReqIfcCntrl outputs_pkgen_ctrl_n_ ();

endinterface 

(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_PktGen_FIFO(Ifc_RapidIO_PktGen_FIFO);

Reg#(InitReqIfcCntrl) rg_datastr_controls <- mkReg(defaultValue);
Wire#(InitiatorReqIfcPkt) wr_InitReqInput <- mkDWire (defaultValue);
Reg#(Bit#(2)) state <- mkReg(0);

// 4 queues are used to store the data and level of priority is assigned to each queues
FIFOF#(QueueData) ff_Highfirst_prio <- mkSizedFIFOF(q_depth);	//queue for the bins ( class of service based on priority) High priority    
FIFOF#(QueueData) ff_Highnext_prio <- mkSizedFIFOF(q_depth);	//queue for the bins ( class of service based on priority) nextHigh priority
FIFOF#(QueueData) ff_Medium_prio <- mkSizedFIFOF(q_depth);  //queue for the bins ( class of service based on priority) medium priority
FIFOF#(QueueData) ff_Low_prio <- mkSizedFIFOF(q_depth);		//queue for the bins ( class of service based on priority) lowest priority
  
//Reg#(Bit#(64)) rg_OutputFtype9DataDelayed1 <- mkReg(0);	// this register will hold the 64 bit outgoing data through the RapidIO fabric
Reg#(DestId) rg_DestId <- mkReg(0); //To store destination Id for continuous and end segment
Reg#(FType9_DataStreamingClass)	rg_Ftype9InputValid <- mkReg(defaultValue);
Reg#(Bool) rg_queue_status <- mkReg(False);
Wire#(Bool) wr_ftype9_activate <- mkDWire(False);
Reg#(DataPkt) rg_data_to_topmod <- mkReg(0);

//Reg#(int) rg_OutputFtype9Vld <- mkReg(0);
Wire#(FType9_DataStreamingClass) wr_Ftype9DataStreamInput <- mkDWire (defaultValue); //To recieve concatenated data


Reg#(QueueData) rg_OutputFtype9Data1 <- mkReg (0);
Reg#(QueueData) rg_OutputFtype9Data <- mkReg (0);
Reg#(QueueData) rg_OutputFtype9DataDelayed1 <- mkReg (0);
Reg#(QueueData) rg_OutputFtype9DataDelayed2 <- mkReg (0);
Reg#(FType9_DataStreamingClass) rg_inputFtype9DataDelayed1 <- mkReg (defaultValue);
Reg#(FType9_DataStreamingClass) rg_inputFtype9DataDelayed2 <- mkReg (defaultValue);
// Registers require to delay the Input Interface signals.. Used as control signals (two registers used for delaying purpose)
Reg#(InitiatorReqIfcPkt) rg_InitReqInput1Delay <- mkReg (defaultValue); // Initiator Req signals - Delayed 1 Clock cycle
Reg#(InitiatorReqIfcPkt) rg_InitReqInput2Delay <- mkReg (defaultValue); // Initiator Req signals - Delayed 2 Clock cycle
Reg#(int) st <- mkReg(4);

// Init Request Data Register
Reg#(Data) rg_InitReqInputData <- mkReg (0);
rule rl_InitReqInterfaceInput2Delay;
    rg_InitReqInput1Delay <= wr_InitReqInput;	// Initiator Request delayed 1 Clock cycle
    rg_InitReqInput2Delay <= rg_InitReqInput1Delay;	// Initiator Request delayed 2 clock cycle
 /*$display("Initreq pkt -> Inside delay wire.. %h",wr_InitReqInput.ireqdata.ireq_data);
 $display("Initreq pkt -> Inside delay register..%h",rg_InitReqInput1Delay.ireqdata.ireq_data);
 $display("Initreq pkt -> Inside delay register.. %h",rg_InitReqInput2Delay.ireqdata.ireq_data);*/
endrule: rl_InitReqInterfaceInput2Delay

rule rl_store_destId(wr_InitReqInput.ireqcntrl.ireq_sof == True);
		rg_DestId <= wr_InitReqInput.ireqdata.ireq_destid;
endrule: rl_store_destId

rule rl_Ftype9Input2Delay(rg_inputFtype9DataDelayed1.ftype == `RIO_FTYPE9_DATASTREAM);
    rg_inputFtype9DataDelayed1 <= wr_Ftype9DataStreamInput;	// Initiator Request delayed 1 Clock cycle
    rg_inputFtype9DataDelayed2 <= rg_inputFtype9DataDelayed1;	// Initiator Request delayed 2 clock cycle
    /*$display("Format 9 -> Inside delay wire.. %h",wr_Ftype9DataStreamInput);
    $display("Format 9 -> Inside delay register.. %h",rg_inputFtype9DataDelayed1);
    $display("Format 9 -> Inside delay register.. %h",rg_inputFtype9DataDelayed2);*/
endrule: rl_Ftype9Input2Delay


rule rl_Ftype9Generation (rg_inputFtype9DataDelayed1.ftype == `RIO_FTYPE9_DATASTREAM);  // Ftype9 Packet 
 	//$display("Inside Rule  for packet creation.. %h", rg_inputFtype9DataDelayed1);
if(rg_InitReqInput1Delay.ireqcntrl.ireq_vld)
 	rg_OutputFtype9Data <= {fn_Dev8Ftype9HeaderCreation (rg_inputFtype9DataDelayed1, rg_DestId[31:24], 8'h00, 2'b00, 2'b01), 8'h00, rg_inputFtype9DataDelayed1.data[63:8], rg_inputFtype9DataDelayed1.data[7:0] ,56'hcccccccccccccc};
endrule

rule r2(rg_inputFtype9DataDelayed1.ftype == `RIO_FTYPE9_DATASTREAM && !(wr_InitReqInput.ireqcntrl.ireq_vld));
rg_OutputFtype9Data <= 0;    
endrule 


rule rl_enque(rg_OutputFtype9Data[187:184] == `RIO_FTYPE9_DATASTREAM); //Ftype9 enque based on priority bits from the Initiator Signals
	//$display("Inside Rule  for enque.. %h",rg_OutputFtype9Data);	
	if(rg_OutputFtype9Data[167:160]==3) begin
		ff_Highfirst_prio.enq(rg_OutputFtype9Data);
		rg_OutputFtype9Data<=0;
	end
	else if(rg_OutputFtype9Data[167:160]==2) begin
		ff_Highnext_prio.enq(rg_OutputFtype9Data);
		rg_OutputFtype9Data<=0;
	end
	else if(rg_OutputFtype9Data[167:160]==1) begin
		ff_Medium_prio.enq(rg_OutputFtype9Data);
		rg_OutputFtype9Data<=0;
	end
	else if(rg_OutputFtype9Data[167:160]==0) begin
		ff_Low_prio.enq(rg_OutputFtype9Data);
		rg_OutputFtype9Data<=0;
	end
	//rg_OutputFtype9Vld <= 0;
endrule
rule rl_deque_delay(rg_OutputFtype9Data[187:184] == `RIO_FTYPE9_DATASTREAM);
	state<=(state+1)%3;
endrule

rule rl_deque(state==0);	//Deque packets based on priority
	if(ff_Highfirst_prio.notEmpty()) begin
		rg_OutputFtype9Data1 <= ff_Highfirst_prio.first();
		ff_Highfirst_prio.deq();
		if(rg_OutputFtype9Data!=0)
			st<=0;
		//$display("Inside Rule  for deque..3");
	end
endrule
rule rl_deque_1(state==0);	
	if(ff_Highnext_prio.notEmpty() && !(ff_Highfirst_prio.notEmpty())) begin
		rg_OutputFtype9Data1 <= ff_Highnext_prio.first();
		ff_Highnext_prio.deq();
			if(rg_OutputFtype9Data!=0)
			st<=0;
		//$display("Inside Rule  for deque..2");
	end
endrule
rule rl_deque_2(state==0);	

	if(ff_Medium_prio.notEmpty() && !(ff_Highnext_prio.notEmpty()) && !(ff_Highfirst_prio.notEmpty())) begin 
		rg_OutputFtype9Data1 <= ff_Medium_prio.first();
		ff_Medium_prio.deq();
		if(rg_OutputFtype9Data!=0)
			st<=0;
	//	$display("Inside Rule  for deque..1");
	end
endrule
rule rl_deque_3(state==0);	

      if(ff_Low_prio.notEmpty() && !(ff_Medium_prio.notEmpty()) && !(ff_Highnext_prio.notEmpty()) && !(ff_Highfirst_prio.notEmpty())) begin
		rg_OutputFtype9Data1 <= ff_Low_prio.first();
		ff_Low_prio.deq();
		if(rg_OutputFtype9Data!=0)
			st<=0;
	//	$display("Inside Rule  for deque..0");
	end
endrule

rule rl_decompose(rg_OutputFtype9Data1!=0 && rg_OutputFtype9Data1[187:184] == `RIO_FTYPE9_DATASTREAM);		//Decomposing packets(segments) into 3 clocks
	if(st == 0) begin
		rg_data_to_topmod <= rg_OutputFtype9Data1[191:128];
		st <=1;
		rg_datastr_controls <= InitReqIfcCntrl {ireq_sof: True,
   			    		    ireq_eof: False,
			    		    ireq_vld: True,
		 	    		    ireq_dsc: False};
	//	$display("Inside Rule decompose...........header: %h",rg_OutputFtype9Data1[191:128]);		
	end
	else if(st == 1) begin
		rg_data_to_topmod <= rg_OutputFtype9Data1[127:64];
		st <=2;
		rg_datastr_controls <= InitReqIfcCntrl {ireq_sof: False,
   			    		    ireq_eof: False,
			    		    ireq_vld: True,
		 	    		    ireq_dsc: False};
	//	$display("Inside Rule decompose...........data: %h",rg_OutputFtype9Data1[127:64]);
	end
	else if(st == 2) begin
		rg_data_to_topmod <= rg_OutputFtype9Data1[63:0];
		st <=0;
		rg_datastr_controls <= InitReqIfcCntrl {ireq_sof: False,
   			    		    ireq_eof: True,
			    		    ireq_vld: True,
		 	    		    ireq_dsc: False};
	//	$display("Inside Rule decompose...........crc: %h",rg_OutputFtype9Data1[63:0]);
	end
endrule

rule rl_decompose_if_zero(rg_OutputFtype9Data1==0);
if(st == 0) begin
		rg_data_to_topmod <= rg_OutputFtype9Data1[191:128];
		st <=1;
		rg_datastr_controls <= InitReqIfcCntrl {ireq_sof: False,
   			    		    ireq_eof: False,
			    		    ireq_vld: False,
		 	    		    ireq_dsc: False};
		end
	else if(st == 1) begin
		rg_data_to_topmod <= rg_OutputFtype9Data1[127:64];
		st <=2;
rg_datastr_controls <= InitReqIfcCntrl {ireq_sof: False,
   			    		    ireq_eof: False,
			    		    ireq_vld: False,
		 	    		    ireq_dsc: False};
		end
	else if(st == 2) begin
		rg_data_to_topmod <= rg_OutputFtype9Data1[63:0];
		st <=0;
	 rg_datastr_controls <= InitReqIfcCntrl {ireq_sof: False,
   			    		    ireq_eof: False,
			    		    ireq_vld: False,
		 	    		    ireq_dsc: False};      
		end
endrule

rule rl_queue_status ;
	if((ff_Low_prio.notEmpty()== False) && (ff_Medium_prio.notEmpty()== False) && (ff_Highnext_prio.notEmpty()== False) && (ff_Highfirst_prio.notEmpty()== False)) 
	rg_queue_status <= True;
else
	rg_queue_status <= False;

endrule

method Action _inputs_FType9DataStreamingClass(FType9_DataStreamingClass pkt);
	wr_Ftype9DataStreamInput <= pkt;
endmethod: _inputs_FType9DataStreamingClass

method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt ireqpkt);
	wr_InitReqInput <= ireqpkt;
	rg_InitReqInputData <= ireqpkt.ireqdata.ireq_data;
endmethod: _inputs_InitReqIfcPkt
/*
method Action _inputs_ftype9_process(Bool value);
		wr_ftype9_activate <= value;
endmethod	*/

// Output Ports as Methods
 method InitReqIfcCntrl outputs_pkgen_ctrl_n_ ();
	return rg_datastr_controls;
 endmethod: outputs_pkgen_ctrl_n_

 method Bool output_queue_status_();
     return rg_queue_status;
endmethod 
method DataPkt outputs_Data_toTop_();
	return rg_data_to_topmod ;
 endmethod: outputs_Data_toTop_

endmodule : mkRapidIO_PktGen_FIFO

endpackage : RapidIO_PktGen_FIFO

/* Explanation for this RapidIO FIFO packet generation for data streaming.
The interface -> Ifc_RapidIO_PktGen_FIFO  has 4 methods where two input methods and two output methods.
--------------------
     method Action _inputs_FType9DataStreamingClass(FType9_DataStreamingClass pkt);
     
				This method accepts a Data Streaming (Type 9 ) packet format which is passed from top module from which the concatenation returned the Type 9 packet format.
---------------------
    method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt ireqpkt);
	
			This method accepts the entire initiator request packet from the top module for accessing the control signals
---------------------			
		 method DataPkt outputs_Data_toTop_();

		This methos will return a final 64 bit output data (Data that leaves rapidIO) that comes out of the queue after comparing the class of service(priority) . The output 64 bit data is returned as segment parts, that is a segment which contains (header , data , CRC ) each 64bit in size is returned every clock cycle. The transmission of segments is not broken in the middle of the segments. Thus interleaving happens only after completion of transmitting CRC of any single segment 
-----------------------
  method InitReqIfcCntrl outputs_pkgen_ctrl_n_ ();

	This method will return control signals that corresponds only to data streaming type 9 and not to any other types.
	
	Which means a single segment is transferred along with sof, eof , valid , dsc  signals.

	A start segment (Header , data , crc ) will be transmitted sequentially, that is header in first clock, data in second clock and crc in third clock. So SOF will be active low, EOF will be active high, VALID will be active low when header is transmitted.

If the data is being transmitted then SOF is Active high, data is Active high, Valid is active low, DSC is active high always since the target request does not use this signal.

If the CRC is being transmitted then SOF will be active high and EOF will be active LOW, VALID will be active low.

This method is called by top module and in the top module this data is sent to the IOPacketGeneration module from where the actual data will be sent out

-----------------------

------------->>>>>REGISTERS<<<<<<<-----------------
1.  rg_datastr_controls  - This register is of type "InitReqIfcCntrl" which will return the necessary control signals to Top module that is required for data streaming.

2. rg_DestId - This register is of type 8 Bits which is defined in DTypes , will hold the destination ID which is to be obtained from the Configuration Registers. This module will use the value is 'h00 since the id is not known.

3. rg_data_to_topmod  - This register is of type 64 bit tha contains the 64bit header,data,crc to be returned to top module. ( Header - 64 bit, data - 64 bit, CRC- 64 bit) 

4. rg_OutputFtype9Data1 - This register is of type QueueData which is 192 Bits type defined in DTypes module. This register will hold the entire header, data, crc wherein in each clock cycle a single 64 bit data is taken from this 192bit register and sent as header, data , crc in consequetive clock cycles. The result 64 bit is copied to "rg_data_to_topmod" Register.

5. rg_data_to_topmod  - This register is of 64Bit type that holds the result from "rg_OutputFtype9Data1" and finally returned inside outputs_Data_toTop_() method.

6. rg_OutputFtype9Data  - This register will have the entire 192 bit segment that is being returned by a function called "fn_Ftype9HeaderCreation(....)" This function is defined in "RapidIO_TxFTypeFunctions" 

7. rg_inputFtype9DataDelayed1  - This register will hold the Logical data streaming packet that is being returned from "wr_Ftype9DataStreamInput" wire . This register is used to create a clock cycle delay in order to prevent the following data from over writing.

8. st   - this register will be set to start sending header when st == 0  data when st ==1 and crc when st==2. 

9. state  - This register will be set for dequeing every three clock cycles, once a segment is set then the state is set to 0 and then again the dequeing is carried out based on priority.

--------------------------------------------------------
FIFOF is used because it provides two methods in addition such as notEmpty() and notFull()

  FIFOF#(QueueData) ff_Highfirst_prio <- mkSizedFIFOF(QueueDepth);	 

			This queue provides the highest class of service bn the highest priority. 
---------
  FIFOF#(QueueData) ff_Highnext_prio <- mkSizedFIFOF(QueueDepth);	

			This queue provides the next higher class of service than the "ff_Highfirst_prio" queue
----------------
  FIFOF#(QueueData) ff_Medium_prio <- mkSizedFIFOF(QueueDepth);  
			
			This queue provides the medium class of service than the highest and highnext
----------------			
  FIFOF#(QueueData) ff_Low_prio <- mkSizedFIFOF(QueueDepth);	

			This queue is the lowest class of service of all and has the lowest priority.
------------

----------------------->>>>>>>>>>>>>>>>>RULES<<<<<<<<<<<------------------
------------------
rule rl_InitReqInterfaceInput2Delay;

		This rule uses the register to store the incomming packet data to create a delay to avoid overwriting of the following packet.

-------------
rule rl_store_destId(wr_InitReqInput.ireqcntrl.ireq_sof == True);
	
		This rule stores the destination ID that is to be stored in all segments. This value is stored because destination ID is received only once , so it should be assigned in all type 9 packets.

------------
rule rl_Ftype9Input2Delay;

		This rule provides delay for logical packet (data streaming packet) inorder to prevent the overwriting of the following data.

------------
rule rl_Ftype9Generation (rg_inputFtype9DataDelayed1.ftype == `RIO_FTYPE9_DATASTREAM);

		This rule will fire once if the type is DataStreaming and it will generate a 192 Bit segment. This rule has a function call that creates one complete segment, that can be a start segment or continuation segment or end segment or a single segment of a single segment
-----------
rule rl_enque;
			
     "	rg_OutputFtype9Data[167:160] "  This bit range from 160 to 167 is the class of service field, By comparing the class of service we are enqueing the data in the appropirate queue. 

			Order of class of service is  3 > 2 > 1 > 0

--------------
 rule rl_deque_delay;
		
				This rule is for preventing the queue from dequeing a higher priority transaction while a lower priority transaction's single segment is half the way transmitted.

This uses a circular logic, for sending a single segment it takes three clock cycles so the dequeing is done for consequtive 3 clock cycles, Then the state is reset to 0. 
---------------
rule rl_deque(state==0); (or) rule rl_deque_1(state==0);  (or) rule rl_deque_2(state==0); (or) rule rl_deque_3(state==0);

			These rule will start dequeing only when the state is reset to zero. This resetting will be done inside " rule rl_deque_delay;"
---------------
	rule rl_decompose_if_zero(rg_OutputFtype9Data1==0);

				This rule will reset all control signals to active high if there are no data in the register.


----------------
rule rl_decompose(rg_OutputFtype9Data1!=0);

			This rule will start sending parts of segments ( header ,data, crc ) in 64bits in each clock

		
----------------		




*/
