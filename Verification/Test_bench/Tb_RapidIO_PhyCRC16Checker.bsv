/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Test Bensh for RapidIO Physical Layer CRC-16 Checker Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- Test Bench for CRC-16 Checker
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
package Tb_RapidIO_PhyCRC16Checker;

import RapidIO_PhyCRC16Checker::*;

(*synthesize*)
(*always_enabled*)
(*always_ready*)


module mkTb_RapidIO_PhyCRC16Checker(Empty);

Ifc_RapidIO_PhyCRC16Checker rio2 <- mkRapidIO_PhyCRC16Checker;


Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);
Reg#(Bit#(16)) out_check <- mkReg (0);



rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 15 )
		$finish (0);
endrule


rule r2(reg_ref_clk ==5); 
rio2._link_rx_sof_n (False);
rio2._link_rx_vld_n (False);
rio2._link_rx_data (144'b110011101010000011110101001010101101001111100110111011111100000100010011011101011010100011011001110010001110111111000000101001010101101010010000);//data + code
rio2._link_rx_eof_n (False);
rio2._link_rx_rem (4'b1110);
$display("Input == 128'11001110101000001111010100101010110100111110011011101111110000010001001101110101101010001101100111001000111011111100000010100101");
$display("CRC Code == 16'0101101010010000");
$display("REM value == 4'b1010");
//rapidio1._link_tx_data(128'hCEA0F52AD3E66EFC11375A8D9C8EFC0A5)a59d56789045a4b984c0
//state <= 1;
endrule:r2


rule r2_1(reg_ref_clk ==10); 
rio2._link_rx_sof_n (False);
rio2._link_rx_vld_n (False);

rio2._link_rx_data (144'b110011101010000011110101001010101101001111100110111011111100000100010011011101011010100011011001110010001110111111000000101001011001110110111110);//data+code

rio2._link_rx_eof_n (False);
rio2._link_rx_rem (4'b0000);
$display("Input == 128'11001110101000001111010100101010110100111110011011101111110000010001001101110101101010001101100111001000111011111100000010100101");
$display("CRC Code == 16'1001110110111110");
$display("REM value == 4'b0000");
//rapidio1._link_tx_data(128'hCEA0F52AD3E66EFC11375A8D9C8EFC0A5)a59d56789045a4b984c0
//state <= 1;
endrule:r2_1


rule r3;
out_check <= rio2.output_CRC16_check();
$display("CRC Check=%b",out_check);

endrule:r3


endmodule:mkTb_RapidIO_PhyCRC16Checker
endpackage:Tb_RapidIO_PhyCRC16Checker
