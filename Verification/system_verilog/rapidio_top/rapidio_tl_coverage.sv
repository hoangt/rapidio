/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
class rapidio_tl_coverage extends uvm_subscriber#(rapidio_tl_sequence_item);

rapidio_tl_sequence_item ttrans;

`uvm_component_utils_begin(rapidio_tl_coverage)
	`uvm_field_object(ttrans,UVM_ALL_ON)
`uvm_component_utils_end

virtual rapidio_interface tl_vif;

//ANALYSIS EXPORTS
 uvm_analysis_imp #(rapidio_tl_sequence_item,rapidio_tl_coverage)dr2cov_tl_export;
 uvm_analysis_imp #(rapidio_tl_sequence_item,rapidio_tl_coverage)mon2cov_tl_export;


function new(string name,uvm_component parent);
super.new(name,parent);
this.ttrans=new();
this.cover_type2_nread_tl_req=new();
this.cover_type2_nread_tl_resp=new();
this.cover_type5_nwrite_tl_req=new();
this.cover_type5_nwrite_tl_resp=new();
this.cover_type5_nwrite_r_tl_req=new();
this.cover_type5_nwrite_r_tl_resp=new();
this.cover_type5_atomic_tl_req=new();
this.cover_type5_atomic_tl_resp=new();
this.cover_type2_atomic_tl_req=new();
this.cover_type2_atomic_tl_resp=new();
this.cover_maintanence_write_tl_req=new();
this.cover_maintanence_write_tl_resp=new();
this.cover_maintanence_read_tl_req=new();
this.cover_maintanence_read_tl_resp=new();
this.cover_maintanence_resp_tl_req=new();
this.cover_maintanence_resp_tl_resp=new();
this.covergroup_tt_hop_count_tl_req=new();
this.covergroup_tt_hop_count_tl_resp=new();



this.dr2cov_tl_export = new("dr2cov_tl_export",this);
this.mon2cov_tl_export = new("mon2cov_tl_export",this);

endfunction : new

event packet2send_tl_driver;
event packet2receive_tl_monitor;


virtual function void build_phase(uvm_phase phase);
super.build_phase(phase);
if(!uvm_config_db#(virtual rapidio_interface)::get(this,"","vif",tl_vif))
`uvm_fatal("TL_COVERAGE",$sformatf("CONFIGURATION NOT HAS BEEN SET"))
else
begin
`uvm_info("TL_COVERAGE",$sformatf("CONFIGURATION HAS BEEN SET"),UVM_HIGH)
end
endfunction : build_phase

//COVERPOINT FOR TRANSPORT TYPE2  
covergroup cover_type2_nread_tl_req@(packet2send_tl_driver);

TRANSACTION : coverpoint ttrans.tt {
 bins bin_incr = {2'b00};
 bins bin_decr = {2'b01};
 bins bin_set = {2'b10};
 bins bin_clr = {2'b11};
 bins bin_ttypes = default;
}

SRCID : coverpoint ttrans.src_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}

TARGETID : coverpoint ttrans.dest_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
   bins b7={[0:255]};
 bins b6 = default;
}

endgroup : cover_type2_nread_tl_req

//COVERPOINT FOR THE LLTYPE TYPE 2 NREAD REQUEST



//COVERPOINT FOR TRANSPORT TYPE2 RESPONSE 
covergroup cover_type2_nread_tl_resp@(packet2receive_tl_monitor );

TRANSACTION : coverpoint ttrans.tt {
 bins bin_incr = {2'b00};
 bins bin_decr = {2'b01};
 bins bin_set = {2'b10};
 bins bin_clr = {2'b11};
 bins bin_ttypes = default;
}

SRCID : coverpoint ttrans.src_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}

TARGETID : coverpoint ttrans.dest_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}

endgroup : cover_type2_nread_tl_resp


//NWRITE REQUEST COVER GROUP
covergroup cover_type5_nwrite_tl_req@(packet2send_tl_driver)  ;

TRANSACTION : coverpoint ttrans.tt {
 bins bin_nwrite = {[0:3]};
 bins bin_ttypes = default;
}

SRCID : coverpoint ttrans.src_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}

endgroup : cover_type5_nwrite_tl_req

//NWRITE RESPONSE COVER GROUP
covergroup cover_type5_nwrite_tl_resp@(packet2receive_tl_monitor)  ;

TRANSACTION : coverpoint ttrans.tt {
 bins bin_nwrite = {[0:3]};
 bins bin_ttypes = default;
}

SRCID : coverpoint ttrans.src_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}

endgroup : cover_type5_nwrite_tl_resp


//PACKET TYPE 5  NWRITE_R RESPONSE COVERGROUP 
covergroup cover_type5_nwrite_r_tl_req @(packet2send_tl_driver)  ;
TRANSACTION : coverpoint ttrans.tt{
 bins bin_nwrite = {[0:3]};
 bins bin_ttypes = default;
}
endgroup : cover_type5_nwrite_r_tl_req

//PACKET TYPE 5  NWRITE_R RESPONSE COVERGROUP 
covergroup cover_type5_nwrite_r_tl_resp @(packet2receive_tl_monitor)  ;
TRANSACTION : coverpoint ttrans.tt {
 bins bin_nwrite = {[0:3]};
 bins bin_ttypes = default;
}
endgroup : cover_type5_nwrite_r_tl_resp

//PACKET TYPE 5  ATOMIC RESPONSE COVERGROUP
covergroup cover_type5_atomic_tl_req@(packet2send_tl_driver)  ; 
 
TRANSACTION : coverpoint ttrans.tt {
 bins bin_swap = {2'b00};
 bins bin_compare_n_swap = {2'b01};
 bins bin_test_n_swap = {2'b10};
 bins bin_ttypes = default;
}
endgroup : cover_type5_atomic_tl_req



//PACKET TYPE 5  ATOMIC RESPONSE COVERGROUP
covergroup cover_type5_atomic_tl_resp @(packet2receive_tl_monitor)  ; 
 
TRANSACTION : coverpoint ttrans.tt {
 bins bin_swap = {2'b00};
 bins bin_compare_n_swap = {2'b01};
 bins bin_test_n_swap = {2'b10};
 bins bin_ttypes = default;
}
endgroup : cover_type5_atomic_tl_resp


//PACKET TYPE 2  ATOMIC REQUEST COVERGROUP
covergroup cover_type2_atomic_tl_req@(packet2send_tl_driver)  ; 
TRANSACTION : coverpoint ttrans.tt{
 bins bin_incr = {2'b00};
 bins bin_decr = {2'b01};
 bins bin_set = {2'b01};
 bins bin_clr = {2'b11};
 bins bin_ttypes = default;
}
endgroup : cover_type2_atomic_tl_req




//PACKET TYPE 2  ATOMIC REQUEST COVERGROUP
covergroup cover_type2_atomic_tl_resp@(packet2receive_tl_monitor)  ; 
TRANSACTION : coverpoint ttrans.tt{
 bins bin_incr = {2'b00};
 bins bin_decr = {2'b01};
 bins bin_set = {2'b10};
 bins bin_clr = {2'b11};
 bins bin_ttypes = default;
}
endgroup : cover_type2_atomic_tl_resp


//TYPE8 MAINTANANCE WRITE REQUEST COVER GROUP
covergroup cover_maintanence_write_tl_req@(packet2send_tl_driver)  ; 

TRANSACTION : coverpoint ttrans.tt{
 bins bin_mwrite = {2'b01};
 bins bin_m_postwrite = {2'b11};
 bins bin_ttypes = default;
}

SRCID : coverpoint ttrans.src_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}
endgroup :  cover_maintanence_write_tl_req

//TYPE8 MAINTANANCE WRITE RESPONSE COVER GROUP
covergroup cover_maintanence_write_tl_resp@(packet2receive_tl_monitor)  ; 

TRANSACTION : coverpoint ttrans.tt{
 bins bin_mwrite = {2'b01};
 bins bin_m_postwrite = {2'b11};
 bins bin_ttypes = default;
}

SRCID : coverpoint ttrans.src_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}
endgroup :  cover_maintanence_write_tl_resp

//TYPE8 MAINTANANCE READ REQUEST COVER GROUP
covergroup cover_maintanence_read_tl_req @(packet2send_tl_driver)  ; 
 TRANSACTION : coverpoint ttrans.tt {
 bins bin_mread = {2'b00};
 bins bin_ttypes = default;
}
TARGETID : coverpoint ttrans.dest_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}
endgroup : cover_maintanence_read_tl_req

//TYPE8 MAINTANANCE READ REQUEST COVER GROUP
covergroup cover_maintanence_read_tl_resp @(packet2receive_tl_monitor)  ; 
 TRANSACTION : coverpoint ttrans.tt{
 bins bin_mread = {2'b00};
 bins bin_ttypes = default;
}
TARGETID : coverpoint ttrans.dest_id {
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}
endgroup : cover_maintanence_read_tl_resp


//MAINTANANCE RESPONSE PACKET 
covergroup cover_maintanence_resp_tl_req @(packet2send_tl_driver)  ; 
 

TRANSACTION : coverpoint ttrans.tt {
 bins bin_mread_resp = {2'b10};
 bins bin_mwrite_resp = {2'b11};
 bins bin_ttypes = default;
}

TARGETID : coverpoint ttrans.dest_id{
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}

endgroup : cover_maintanence_resp_tl_req

//MAINTANANCE RESPONSE PACKET 
covergroup cover_maintanence_resp_tl_resp @(packet2receive_tl_monitor)  ; 
 

TRANSACTION : coverpoint ttrans.tt {
 bins bin_mread_resp = {2'b10};
 bins bin_mwrite_resp = {2'b11};
 bins bin_ttypes = default;
}

TARGETID : coverpoint ttrans.dest_id{
 bins b1 = {0};
 bins b2 = {255};
 bins b3 = {[2:5]};
 bins b4 = {[123:125]};
 bins b5 = {[251:254]};
 bins b6 = default;
}

endgroup : cover_maintanence_resp_tl_resp

//HOPCOUNT RESopnase
covergroup covergroup_tt_hop_count_tl_req @(packet2send_tl_driver)  ; 
 
//option.goal = 100;

TT : coverpoint ttrans.tt {
option.auto_bin_max=4;

}

HOP_COUNT : coverpoint ttrans.hop_count {
option.auto_bin_max=8;

}
endgroup : covergroup_tt_hop_count_tl_req


//HOPCOUNT RESopnase
covergroup covergroup_tt_hop_count_tl_resp @(packet2receive_tl_monitor)  ; 
 
//option.goal = 100;

TT : coverpoint ttrans.tt {
option.auto_bin_max=4;

}

HOP_COUNT : coverpoint ttrans.hop_count {
option.auto_bin_max=8;

}
endgroup : covergroup_tt_hop_count_tl_resp


// Implementation of an write funciton //
virtual function void write(rapidio_tl_sequence_item t);
this.ttrans = t;
this.cover_type2_nread_tl_req.sample();
this.cover_type2_nread_tl_resp.sample();
this.cover_type5_nwrite_tl_req.sample();
this.cover_type5_nwrite_tl_resp.sample();
this.cover_type5_nwrite_r_tl_req.sample();
this.cover_type5_nwrite_r_tl_resp.sample();
this.cover_type5_atomic_tl_req.sample();
this.cover_type5_atomic_tl_resp.sample();
this.cover_type2_atomic_tl_req.sample();
this.cover_type2_atomic_tl_resp.sample();
this.cover_maintanence_write_tl_req.sample();
this.cover_maintanence_write_tl_resp.sample();
this.cover_maintanence_read_tl_req.sample();
this.cover_maintanence_read_tl_resp.sample();
this.cover_maintanence_resp_tl_req.sample();
this.cover_maintanence_resp_tl_resp.sample();
this.covergroup_tt_hop_count_tl_req.sample();
this.covergroup_tt_hop_count_tl_resp.sample();
endfunction 

endclass : rapidio_tl_coverage


